/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.patient;

import dto.appointment.AppointmentDTO;
import exceptions.ResourceNotFound;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.appointment.Appointment;
import models.user.patient.Patient;
import repository.appointment.appointment.AppointmentFileRepo;
import repository.appointment.appointment_time_slot.AppointmentTimeSlotFileRepo;
import repository.appointment.appointment_type.MedicalAppointmentTypeFileRepo;
import repository.appointment.time_slot.TimeSlotFileRepo;
import repository.notification.NotificationFileRepo;
import repository.room.room.RoomFileRepo;
import repository.user.user.UserFileRepo;
import service.appointment.AppointmentService;
import service.appointment.IAppointmentService;
import utility.FileManager;

/**
 *
 * @author stefan
 */
public class ViewAppointmentsMenu {

    private static ViewAppointmentsMenu viewAppointmentsMenu = null;
    IAppointmentService appointmentService = new AppointmentService(new AppointmentFileRepo(FileManager.APPOINTMENT_URI), new UserFileRepo(FileManager.USER_URI), new RoomFileRepo(FileManager.ROOM_URI), new MedicalAppointmentTypeFileRepo(FileManager.MEDICAL_APPOINTMENT_URI), new TimeSlotFileRepo(FileManager.TIME_SLOT_URI), new AppointmentTimeSlotFileRepo(FileManager.APPOINTMENT_TIMESLOT_URI), new NotificationFileRepo(FileManager.NOTIFICATION_URI));

    private ViewAppointmentsMenu() {
    }

    public static ViewAppointmentsMenu getInstance() {
        if (viewAppointmentsMenu == null) {
            viewAppointmentsMenu = new ViewAppointmentsMenu();
        }
        return viewAppointmentsMenu;
    }

    public void showAppointments(Patient patient) {
        try {
            System.out.println("Appointments");
            for (AppointmentDTO adto : appointmentService.getAllByPatient(patient.getId())) {
                printAppointment(adto);
            }
            System.out.println("-------------------------");
        } catch (ResourceNotFound ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void printAppointment(AppointmentDTO appointment) {
        System.out.println(appointment.toString());
    }
}
