/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.patient;

import dto.appointment.AppointmentDTO;
import dto.appointment.PriorityTypeDTO;
import exceptions.CustomException;
import exceptions.ResourceNotFound;
import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.appointment.Appointment;
import models.appointment.AppointmentStatus;
import models.appointment.TimeSlot;
import models.appointment.type.AppointmentType;
import models.appointment.type.MedicalAppointmentType;
import models.user.User;
import models.user.doctor.Doctor;
import models.user.doctor.Specialisation;
import models.user.patient.Patient;
import repository.appointment.appointment.AppointmentFileRepo;
import repository.appointment.appointment_time_slot.AppointmentTimeSlotFileRepo;
import repository.appointment.appointment_type.MedicalAppointmentTypeFileRepo;
import repository.appointment.time_slot.TimeSlotFileRepo;
import repository.notification.NotificationFileRepo;
import repository.room.room.RoomFileRepo;
import repository.user.doctor.DoctorSpecialisationFileRepo;
import repository.user.doctor.SpecialisationFileRepo;
import repository.user.user.UserFileRepo;
import service.appointment.AppointmentService;
import service.appointment.IAppointmentService;
import service.appointment.IMedicalAppointmentTypeService;
import service.appointment.MedicalAppointmentTypeService;
import service.specialisation.DoctorSpecialisationService;
import service.specialisation.IDoctorSpecialisationService;
import service.specialisation.ISpecialisationService;
import service.specialisation.SpecialisationService;
import utility.CustomScanner;
import utility.FileManager;

/**
 *
 * @author stefan
 */
public class AppointmentManagmentMenu {

    private static AppointmentManagmentMenu appointmentManagmentMenu = null;
    private IAppointmentService appointmentService = new AppointmentService(new AppointmentFileRepo(FileManager.APPOINTMENT_URI), new UserFileRepo(FileManager.USER_URI), new RoomFileRepo(FileManager.ROOM_URI), new MedicalAppointmentTypeFileRepo(FileManager.MEDICAL_APPOINTMENT_URI), new TimeSlotFileRepo(FileManager.TIME_SLOT_URI), new AppointmentTimeSlotFileRepo(FileManager.APPOINTMENT_TIMESLOT_URI), new NotificationFileRepo(FileManager.NOTIFICATION_URI));
    private ISpecialisationService specialisationService = new SpecialisationService(new SpecialisationFileRepo(FileManager.SPECIALISATION_URI));
    private IDoctorSpecialisationService doctorSpecialisationService = new DoctorSpecialisationService(new DoctorSpecialisationFileRepo(FileManager.DOCTOR_SPECIALISATION_URI), new SpecialisationFileRepo(FileManager.SPECIALISATION_URI), new UserFileRepo(FileManager.USER_URI));
    private IMedicalAppointmentTypeService medicalAppointmentTypeService = new MedicalAppointmentTypeService(new MedicalAppointmentTypeFileRepo(FileManager.MEDICAL_APPPOINTMENT_TYPE_URI));

    private AppointmentManagmentMenu() {
    }

    public static AppointmentManagmentMenu getInstance() {
        if (appointmentManagmentMenu == null) {
            appointmentManagmentMenu = new AppointmentManagmentMenu();
        }
        return appointmentManagmentMenu;
    }

    public void updateAppointment(Patient patient) {
        try {
            List<AppointmentDTO> adtos = appointmentService.getActiveByPatient(patient.getId());
            printAppointments(adtos);
            Appointment appointment = pickAppointment(adtos);
            LocalDate localDate = LocalDate.now();
            localDate = localDate.minusDays(2);
            if (appointment.getDate().compareTo(localDate) < 1) {
                appointment.setAppointmentStatus(AppointmentStatus.CANCELED);
                appointmentService.update(appointment);
                System.out.println("-------------------------");
            } else {
                System.out.println("Datum ne moze biti otkazan!");
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void schedule(Patient patient) {
        try {
            TimeSlot slot = getTimeSlot();
            LocalDate latesDate = getLatestDate();
            Specialisation specialisation = getSpecialisation();
            Doctor doctor = getDoctor(specialisation);
            MedicalAppointmentType appointmentType = getMedicalType();
            PriorityTypeDTO priorityTypeDTO = getPriorityTypeDTO();
            appointmentService.createAppointment(doctor, slot, latesDate, appointmentType.getId(), priorityTypeDTO, patient);
            System.out.println("Zakazan pregled");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private TimeSlot getTimeSlot() throws Exception {
        LocalTime startTime = CustomScanner.getLocalTime("Unesi pocetak termina");
        LocalTime endTime = startTime.plusMinutes(30);
        return new TimeSlot(startTime, endTime);
    }

    private LocalDate getLatestDate() throws ParseException, CustomException {
        Date latestDate = CustomScanner.getDate("Unesi najkasniji datum");
        //check if date is tomorrow or later
        if (latestDate.compareTo(new Date()) < 0) {
            throw new CustomException("Datum mora biti u buducnosti");
        }
        return Instant.ofEpochMilli(latestDate.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    private Specialisation getSpecialisation() throws IOException, CustomException {
        List<Specialisation> list = specialisationService.getAll();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + ") " + list.get(i).getName());
        }
        int choice = CustomScanner.nextInt("Izaberite specijalizaciju: ");
        if (choice < 0 || choice > list.size() - 1) {
            throw new CustomException("Nepravilan izbor");
        }
        return list.get(choice);
    }

    private Doctor getDoctor(Specialisation specialisation) throws ResourceNotFound, CustomException, IOException {
        List<User> users = doctorSpecialisationService.getDoctors(specialisation.getId());
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i + ") " + users.get(i).getFirstName() + " " + users.get(i).getLastName());
        }
        int choice = CustomScanner.nextInt("Izaberite doktora: ");
        if (choice < 0 || choice > users.size() - 1) {
            throw new CustomException("Nepravilan izbor");
        }
        return (Doctor) users.get(choice);
    }

    private MedicalAppointmentType getMedicalType() throws ResourceNotFound {
        return medicalAppointmentTypeService.getByAppointmentType(AppointmentType.EXAMINATION);
    }

    private void printAppointments(List<AppointmentDTO> adtos) {

        for (int i = 0; i < adtos.size(); i++) {
            System.out.println(i + ") " + adtos.get(i).toString());
        }
    }

    private Appointment pickAppointment(List<AppointmentDTO> adtos) throws Exception {
        int choice = CustomScanner.nextInt("Izaberi pregled ciji ces termin otkazati");
        if (choice < 0 || choice > adtos.size() - 1) {
            throw new ArrayIndexOutOfBoundsException("Izabrali ste van opsega!");
        }
        return appointmentService.getById(adtos.get(choice).getId());
    }

    private PriorityTypeDTO getPriorityTypeDTO() throws IOException, CustomException {
        List<PriorityTypeDTO> enumValues = Arrays.asList(PriorityTypeDTO.values());
        for (int i = 0; i < enumValues.size(); i++) {
            System.out.println(i + ") " + enumValues.get(i).name());
        }
        int choice = CustomScanner.nextInt("Izaberite prioritet: ");
        if (choice < 0 || choice > enumValues.size() - 1) {
            throw new CustomException("Nepravilan izbor");
        }
        return enumValues.get(choice);
    }

}
