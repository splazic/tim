/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.patient;

import models.user.User;
import models.user.patient.Patient;
import utility.CustomScanner;

/**
 *
 * @author stefan
 */
public class PatientMenu {

    private static PatientMenu patientMenu = null;

    private PatientMenu() {

    }

    public static PatientMenu getInstance() {
        if (patientMenu == null) {
            patientMenu = new PatientMenu();
        }
        return patientMenu;
    }

    public void showMenu(User user) {
        int input = 0;
        Patient patient = (Patient) user;
        do {
            try {
                System.out.println("1 - Pregled zdravstvenog kartona");
                System.out.println("2 - Lista zakazanih pregleda");
                System.out.println("3 - Otkazi pregled");
                System.out.println("4 - Zakazi pregled");
                System.out.println("0 - Odjavi se.");
                input = CustomScanner.nextInt("Vas izbor: ");
                checkInput(input, patient);
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        } while (input != 0);
    }

    private static void checkInput(int input, Patient patient) {
        AppointmentManagmentMenu appointmentManagmentMenu;
        switch (input) {
            case 1:
                ViewMedicalRecordMenu viewMedicalRecordMenu = ViewMedicalRecordMenu.getInstance();
                viewMedicalRecordMenu.showPatientData(patient);
                break;
            case 2:
                ViewAppointmentsMenu viewAppointmentsMenu = ViewAppointmentsMenu.getInstance();
                viewAppointmentsMenu.showAppointments(patient);
                break;
            case 3:
                appointmentManagmentMenu = AppointmentManagmentMenu.getInstance();
                appointmentManagmentMenu.updateAppointment(patient);
                break;
            case 4:
                appointmentManagmentMenu = AppointmentManagmentMenu.getInstance();
                appointmentManagmentMenu.schedule(patient);
                break;
            default:
                System.out.println("\nPogresan izbor.\n");
        }
    }
}
