/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.patient;

import dto.medical_record.MedicalRecordDTO;
import exceptions.ResourceNotFound;
import models.user.patient.Patient;
import repository.medical_record.medical_rec_item.MedicalRecItemFileRepo;
import repository.medical_record.medical_record.MedicalRecordFileRepo;
import service.medical_record.IMedicalRecordService;
import service.medical_record.MedicalRecordItemService;
import service.medical_record.MedicalRecordService;
import utility.FileManager;

/**
 *
 * @author stefan
 */
public class ViewMedicalRecordMenu {

	private static ViewMedicalRecordMenu viewMedicalRecordMenu = null;
	IMedicalRecordService medicalRecordService = new MedicalRecordService(
			new MedicalRecordFileRepo(FileManager.MEDICALRECORD_URI),
			new MedicalRecordItemService(new MedicalRecItemFileRepo(FileManager.MEDICAL_RECORD_ITEM_URI)));

	private ViewMedicalRecordMenu() {
	}

	public static ViewMedicalRecordMenu getInstance() {
		if (viewMedicalRecordMenu == null) {
			viewMedicalRecordMenu = new ViewMedicalRecordMenu();
		}
		return viewMedicalRecordMenu;
	}

	public void showPatientData(Patient patient) {
		try {
			MedicalRecordDTO medicalRecordDTO = medicalRecordService
					.getMedicalRecordWithItems(patient.getMedicalRecordId());
			printMedicalRecord(medicalRecordDTO);
		} catch (ResourceNotFound e) {
			System.out.println(e.getMessage());
		          }
	}

	private void printMedicalRecord(MedicalRecordDTO medicalRecord) {
		System.out.println(medicalRecord.toString());
	}
}
