package menu.administrator;

import models.user.User;
import utility.CustomScanner;

public class AdministratorMenu {

    private static AdministratorMenu administratorMenu = null;
    private static RoomManagmentMenu roomManagmentMenu = null;

    private AdministratorMenu() {
    }

    public static AdministratorMenu getInstance() {
        if (administratorMenu == null) {
            administratorMenu = new AdministratorMenu();
        }
        return administratorMenu;
    }

    public void showMenu(User user) {
        int input = 0;
        do {
            System.out.println("1 - Dodavanje korisnika.");
            System.out.println("2 - Azuriranje korisnika.");
            System.out.println("3 - Brisanje korisnika.");
            System.out.println("4 - Dodavanje prostorije.");
            System.out.println("5 - Brisanje prostorije.");
            System.out.println("6 - Reaktivacija korisnika u sistem");
            System.out.println("0 - Odjavi se.");
            try {
                input = CustomScanner.nextInt("Vas izbor: ");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            checkInput(input);
        } while (input != 0);
    }

    private void checkInput(int input) {
        switch (input) {
            case 0:
                break;
            case 1:
                InsertUserMenu insertUserMenu = InsertUserMenu.getInstance();
                insertUserMenu.insertUser();
                break;
            case 2:
                UserUpdateMenu userUpdateMenu = UserUpdateMenu.getInstance();
                userUpdateMenu.showMenu();
                break;
            case 3:
                ActivateDeactivateUserMenu activateDeactivateUserMenu = ActivateDeactivateUserMenu.getInstance();
                activateDeactivateUserMenu.deactivateUser();
                break;
            case 4:
                roomManagmentMenu = RoomManagmentMenu.getInstance();
                roomManagmentMenu.addRoom();
                break;
            case 5:
                roomManagmentMenu = RoomManagmentMenu.getInstance();
                roomManagmentMenu.deleteRoom();
                break;
            case 6:
                activateDeactivateUserMenu = ActivateDeactivateUserMenu.getInstance();
                activateDeactivateUserMenu.activateUser();
                break;
            default:
                System.out.println("\nPogresan izbor.\n");
        }
    }
}
