/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.administrator;

import exceptions.ResourceNotFound;
import factory.UserServiceFactory;

import javax.management.InvalidAttributeValueException;
import models.user.Manager;
import models.user.Secretary;
import models.user.doctor.Doctor;
import service.user.IUserService;
import utility.CustomScanner;
import utility.Validation;

/**
 *
 * @author stefan
 */
public class InsertUserMenu {

    private static InsertUserMenu userInsertMenu = null;
    private Validation validation = null;
    private IUserService userService;

    private InsertUserMenu() {
    	UserServiceFactory userServiceFactory = new UserServiceFactory();
    	this.userService = userServiceFactory.getUserService();
    }

    public static InsertUserMenu getInstance() {
        if (userInsertMenu == null) {
            userInsertMenu = new InsertUserMenu();
        }
        return userInsertMenu;
    }

    public void insertUser() {
        try {
            int choice = CustomScanner.getChar(new String[]{"1 Unesi lekara", "2 Unesi sekretara", "3 Unesi upravnika",
                "0 Vratie se na predhodni meni"});
            switch (choice) {
                case '1':
                    insertDoctor();
                    break;
                case '2':
                    insertSecretary();
                    break;
                case '3':
                    insertManager();
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void insertManager() throws ResourceNotFound, InvalidAttributeValueException {
        String[] userData = getUserData();
        Manager manager = new Manager(userData[0], userData[1], userData[2], userData[3]);
        userService.save(manager);
    }

    private void insertSecretary() throws ResourceNotFound, InvalidAttributeValueException {
        String[] userData = getUserData();
        Secretary secretary = new Secretary(userData[0], userData[1], userData[2], userData[3]);
        userService.save(secretary);
    }

    private void insertDoctor() throws InvalidAttributeValueException, ResourceNotFound {
        String[] userData = getUserData();
        Doctor doctor = new Doctor(userData[0], userData[1], userData[2], userData[3]);
        userService.save(doctor);
    }

    private String[] getUserData() throws ResourceNotFound {
        String username = getUsername();
        userService.getByUsername(username);
        String password = getPassword();
        String fName = getFirstName();
        String lName = getLastName();
        return new String[]{username, password, fName, lName};
    }

    private String getUsername() {
        validation = Validation.getInstance();
        String username = CustomScanner.nextLine("Korisnicko ime");
        if (!validation.username(username)) {
            System.out.println("Pogresan format korisnickog imena");
            return null;
        }
        return username;
    }

    private String getPassword() {
        validation = Validation.getInstance();
        String password = CustomScanner.nextLine("Lozinka");
        if (!validation.length(password, 8, 25)) {
            System.out.println("Duzina lozinke mora biti izmedju 8 i 25 karaktera");
            return null;
        }
        return password;
    }

    private String getLastName() {
        validation = Validation.getInstance();
        String lastName = CustomScanner.nextLine("Prezime");
        if (!validation.length(lastName, 1, 20) && validation.isStringOnlyAlphabet(lastName)) {
            System.out.println("Pogresan format prezimena");
            return null;
        }
        return lastName;
    }

    private String getFirstName() {
        validation = Validation.getInstance();
        String firstName = CustomScanner.nextLine("Ime");
        if (!validation.length(firstName, 1, 20) && validation.isStringOnlyAlphabet(firstName)) {
            System.out.println("Pogresan format imena");
            return null;
        }
        return firstName;
    }

}
