/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.administrator;

import java.util.Date;
import java.util.List;

import factory.RoomServiceFactory;
import models.room.Room;
import models.room.RoomType;
import service.room.IRoomService;
import utility.CustomScanner;

/**
 *
 * @author stefan
 */
public class RoomManagmentMenu {

    public static RoomManagmentMenu roomManagmentMenu = null;
    public IRoomService roomService;

    private RoomManagmentMenu() {
    	RoomServiceFactory roomServiceFactory = new RoomServiceFactory();
    	roomService = roomServiceFactory.getRoomService();
    }

    public static RoomManagmentMenu getInstance() {
        if (roomManagmentMenu == null) {
            roomManagmentMenu = new RoomManagmentMenu();
        }
        return roomManagmentMenu;
    }

    public void addRoom() {
        try {
            int roomFloor = getRoomFloor();
            int roomNumber = getRoomNumber();
            String roomId = roomFloor + "-" + roomNumber;
            RoomType roomType = getRoomType();
            this.roomService.save(new Room(roomId, true, roomType, new Date(), new Date()));
            System.out.println("Soba je uneta");
            System.out.println("----------------------------------");
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void deleteRoom() {
        try {
            Room room = getRoom();
            this.roomService.delete(room);
            System.out.println("Soba je uspesno obrisana");
            System.out.println("----------------------------------");
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    private Room getRoom() throws Exception {
        int choice;
        List<Room> rooms = roomService.getAll();
        printRooms(rooms);
        choice = CustomScanner.nextInt("Izaberite sobu za brisanje:");
        if (choice < 0 || choice >= rooms.size()) {
            throw new NumberFormatException("Vrednost mora biti izmedju: 0 i " + (rooms.size() - 1));
        }
        return rooms.get(choice);
    }

    private void printRooms(List<Room> rooms) {
        for (int i = 0; i < rooms.size(); i++) {
            System.out.println(i + " " + rooms.get(i).toString());
        }
    }

    private RoomType getRoomType() throws Exception {
        RoomType[] roomTypes = roomService.getRoomTypes();
        //int roomOption;
        int roomChoice;
        System.out.println("Ponudjeni tipovi sobe");
        for (int i = 0; i < roomTypes.length; i++) {
            System.out.println(i + ". " + roomTypes[i]);
        }
        roomChoice = CustomScanner.nextInt("Izaberite sobu");
        if (roomChoice < 0 || roomChoice >= roomTypes.length) {
            throw new NumberFormatException("Uneta opcija nije validna");
        }
        return roomTypes[roomChoice];
    }

    private int getRoomFloor() throws Exception {
        int roomFloor = CustomScanner.nextInt("Unesite sprat");
        return roomFloor;
    }

    private int getRoomNumber() throws Exception {
        int roomNumber = CustomScanner.nextInt("Unesite redni broj sobe");
        return roomNumber;

    }
}
