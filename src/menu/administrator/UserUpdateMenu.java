package menu.administrator;

import exceptions.ResourceNotFound;
import factory.UserServiceFactory;

import java.io.IOException;

import javax.management.InvalidAttributeValueException;
import models.user.User;
import service.user.IUserService;
import utility.CustomScanner;

public class UserUpdateMenu {

    private static UserUpdateMenu userUpdateMenu = null;
    
    private IUserService userService;

    private UserUpdateMenu() {
    	UserServiceFactory userServiceFactory = new UserServiceFactory();
    	userService = userServiceFactory.getUserService();
    }

    ;

	public static UserUpdateMenu getInstance() {
        if (userUpdateMenu == null) {
            userUpdateMenu = new UserUpdateMenu();
        }
        return userUpdateMenu;
    }

    public void showMenu() {
        try {
            // TODO Auto-generated method stub
            String userName = CustomScanner.nextLine("Unesi korisnicko ime: ");
            User user = userService.getByUsername(userName);
            updateUserMenu(user);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void updateUserMenu(User user){
        int input = 0;
        do {
            System.out.println(user.toString());
            System.out.println("1 - Izmeni korisnicko ime.");
            System.out.println("2 - Izmeni lozinku");
            System.out.println("3 - Izmeni ime.");
            System.out.println("4 - Izmeni prezime.");
            System.out.println("5 - Sacuvaj izmene.");
            System.out.println("0 - Nazad.");
            try {
				input = CustomScanner.nextInt("Vas izbor: ");
				checkInput(input, user);
			} catch (InvalidAttributeValueException | ResourceNotFound | IOException e) {
				System.err.println(e.getMessage());
			}
        } while (input != 0 && input != 5);
    }

    private void checkInput(int input, User user) throws ResourceNotFound, InvalidAttributeValueException {
        switch (input) {
            case 0:
                break;
            case 1:
                updateUserName(user);
                break;
            case 2:
                updatePassword(user);
                break;
            case 3:
                updateFirstName(user);
                break;
            case 4:
                updateLastName(user);
                break;
            case 5:
                userService.update(user);
                System.out.println("\nIzmene su sacuvane.\n");
                break;
            default:
                System.out.println("\nPogresan izbor.\n");
        }
    }

    private void updateFirstName(User user) {
        String firstName = CustomScanner.nextLine("Unesi ime: ");
        user.setFirstName(firstName);
    }

    private void updateLastName(User user) {
        String lastName = CustomScanner.nextLine("Unesi Prezime: ");
        user.setLastName(lastName);
    }

    private void updateUserName(User user) {
        String userName = CustomScanner.nextLine("Unesi korisnicko ime: ");
        user.setUsername(userName);
    }

    private void updatePassword(User user) {
        String password = CustomScanner.nextLine("Unesi lozinku: ");
        user.setPassword(password);
    }
}
