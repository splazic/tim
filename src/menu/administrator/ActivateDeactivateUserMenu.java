package menu.administrator;

import java.util.List;

import javax.management.InvalidAttributeValueException;

import exceptions.ResourceNotFound;
import factory.UserServiceFactory;

import java.io.IOException;
import models.user.User;
import service.user.IUserService;
import utility.CustomScanner;

public class ActivateDeactivateUserMenu {

    public static ActivateDeactivateUserMenu activateDeactivateUserMenu = null;
    private IUserService userService;

    private ActivateDeactivateUserMenu() {
    	UserServiceFactory userServiceFactory = new UserServiceFactory();
    	this.userService = userServiceFactory.getUserService();
    }

    public static ActivateDeactivateUserMenu getInstance() {
        if (activateDeactivateUserMenu == null) {
            activateDeactivateUserMenu = new ActivateDeactivateUserMenu();
        }
        return activateDeactivateUserMenu;
    }

    public void activateUser() {
        User user;
        try {
            user = findByUsername();
            if (!user.isDeleted()) {
                throw new ResourceNotFound(("Ne postoji korisnik sa tim korisnickim imenom"));
            }
            user.setDeleted(false);
            userService.update(user);
            System.out.println("Korisnik je uspesno vracen u sistem");
        } catch (InvalidAttributeValueException e) {
            System.out.println(e.getMessage());
        } catch (ResourceNotFound e) {
            System.out.println(e.toString());
        }
    }

    public void deactivateUser() {
        try {
            User user = getUser();
            userService.delete(user);
            System.out.println(user.getUsername() + " je uspesno izbrisan");
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }

    }

    private User getUser() throws NumberFormatException, IOException, Exception {
        int choice;
        List<User> users = userService.getActiveUserListWithoutAdmin();
        printUsers(users);
        choice = CustomScanner.nextInt("Izaberite korisnika koga zelite da obrisete:");
        if (choice < 0 || choice >= users.size()) {
            throw new NumberFormatException("Uneta je nedozvoljena vrednost");
        }
        return users.get(choice);
    }

    private void printUsers(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            System.out.println(i + " " + user.toString());
        }
    }

    private User findByUsername() throws ResourceNotFound {
        String username = getUsername();
        User user = userService.getByUsername(username);
        return user;
    }

    private String getUsername() {
        String username = CustomScanner.nextLine("Korisnicko ime");
        return username;
    }
}
