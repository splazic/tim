package menu.manager;

import java.io.IOException;

import exceptions.ResourceNotFound;
import factory.EquipmentServiceFactory;
import models.equipment.HospitalEquipment;
import service.equipment.IHospitalEquipmentService;
import utility.CustomScanner;

class DeleteEquipmentMenu {
	private static DeleteEquipmentMenu deleteEquipmentMenu;
	
	IHospitalEquipmentService equipmentService;

	private DeleteEquipmentMenu() {
		EquipmentServiceFactory equipmentServiceFactory = new EquipmentServiceFactory();
		this.equipmentService = equipmentServiceFactory.getEquipmentService();
	}

	public static DeleteEquipmentMenu getInstance() {
		if (deleteEquipmentMenu == null) {
			deleteEquipmentMenu = new DeleteEquipmentMenu();
		}
		return deleteEquipmentMenu;
	}

	public void showMenu() {
		try {
			int equId = CustomScanner.nextInt("Unesi id opreme");
			HospitalEquipment equipment = equipmentService.getById(equId);
			System.out.println(equipment.toString());
			boolean delete = CustomScanner.getBoolean("Da li ste sigurni da zelite izbrisati opremu iz sistema? ");
			if (delete) {
				equipmentService.delete(equipment);
				System.out.println("\nOprema je izbrisana iz sistema.\n");
			}
		} catch (IOException | ResourceNotFound e) {
			System.err.println(e.getMessage());
		}
	}
}
