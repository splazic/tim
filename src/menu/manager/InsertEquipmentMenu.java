package menu.manager;

import java.io.IOException;

import javax.management.InvalidAttributeValueException;

import factory.EquipmentServiceFactory;
import models.equipment.HospitalEquipment;
import service.equipment.IHospitalEquipmentService;
import utility.CustomScanner;

class InsertEquipmentMenu {
	private static InsertEquipmentMenu insertEquipmentMenu = null;

	private IHospitalEquipmentService equipmentService;

	private InsertEquipmentMenu() {
		EquipmentServiceFactory equipmentServiceFactory = new EquipmentServiceFactory();
		equipmentService = equipmentServiceFactory.getEquipmentService();
	}

	public static InsertEquipmentMenu getInstance() {
		if (insertEquipmentMenu == null)
			insertEquipmentMenu = new InsertEquipmentMenu();
		return insertEquipmentMenu;
	}

	public void showMenu() {
		try {
			HospitalEquipment hEquipment = getEquipment();
			equipmentService.save(hEquipment);
			System.out.println("\nOprema je sacuvana.\n");
		} catch (InvalidAttributeValueException | IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private HospitalEquipment getEquipment() throws IOException {
		HospitalEquipment hEquipment = new HospitalEquipment();
		hEquipment.setName(CustomScanner.nextLine("Unesi ime operme: "));
		hEquipment.setDescription(CustomScanner.nextLine("Unesi opis opreme: "));
		hEquipment.setTotalEquipment(CustomScanner.nextInt("Unesi kolicinu: "));
		return hEquipment;
	}
}
