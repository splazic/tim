package menu.manager;

import java.io.IOException;

import models.user.User;
import utility.CustomScanner;

public class ManagerMenu {
	private static ManagerMenu managerMenu = null;

	private ManagerMenu() {

	}

	public static ManagerMenu getInstance() {
		if (managerMenu == null)
			managerMenu = new ManagerMenu();
		return managerMenu;
	}

	public void showMenu(User user) {
		int input = 0;
		do {
			System.out.println("1 - Renoviranje prostorije.");
			System.out.println("2 - Pretraga prostorija.");
			System.out.println("3 - Dodavanje nove opreme u sistem.");
			System.out.println("4 - Azuriranje opreme u sistemu.");
			System.out.println("5 - Brisanje opreme iz sistema.");
			System.out.println("6 - Azuriranje informacija o lekarima.");
			System.out.println("7 - Generisanje izvestaja.");
			System.out.println("0 - Odjavi se.");
			try {
				input = CustomScanner.nextInt("Vas izbor: ");
				checkInput(input, user);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		} while (input != 0);
	}

	private static void checkInput(int input, User user) {
		switch (input) {
		case 0:
			break;
		case 1:
			RoomRenovationMenu roomRenovationMenu = RoomRenovationMenu.getInstance();
			roomRenovationMenu.showMenu(user);
			break;
		case 2:
			break;
		case 3:
			InsertEquipmentMenu insertEquipmentMenu = InsertEquipmentMenu.getInstance();
			insertEquipmentMenu.showMenu();
			break;
		case 4:
			UpdateEquipmentMenu updateEquipmentMenu = UpdateEquipmentMenu.getInstance();
			updateEquipmentMenu.showMenu();
			break;
		case 5:
			DeleteEquipmentMenu deleteEquipmentMenu = DeleteEquipmentMenu.getInstance();
			deleteEquipmentMenu.showMenu();
			break;
		case 6:
			break;
		case 7:
			break;
		default:
			System.out.println("\nPogresan izbor.\n");
		}
	}
}
