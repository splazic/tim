package menu.manager;

import java.io.IOException;

import javax.management.InvalidAttributeValueException;

import exceptions.ResourceNotFound;
import factory.EquipmentServiceFactory;
import models.equipment.HospitalEquipment;
import service.equipment.IHospitalEquipmentService;
import utility.CustomScanner;

class UpdateEquipmentMenu {
	private static UpdateEquipmentMenu updateEquipmentMenu = null;

	private IHospitalEquipmentService equipmentService;

	private UpdateEquipmentMenu() {
		EquipmentServiceFactory equipmentServiceFactory = new EquipmentServiceFactory();
		this.equipmentService = equipmentServiceFactory.getEquipmentService();
	}

	public static UpdateEquipmentMenu getInstance() {
		if (updateEquipmentMenu == null)
			updateEquipmentMenu = new UpdateEquipmentMenu();
		return updateEquipmentMenu;
	}

	public void showMenu() {
		try {
			int equId = CustomScanner.nextInt("Unesi id opreme: ");
			HospitalEquipment hEquipment = equipmentService.getById(equId);
			HospitalEquipment hospitalEquipment = new HospitalEquipment(hEquipment);
			updateEquipment(hospitalEquipment);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (ResourceNotFound e) {
			System.out.println(e.getMessage());
		}
	}

	public void updateEquipment(HospitalEquipment hEquipment) {
		int input = 0;
		do {
			printEquipment(hEquipment);
			System.out.println("\n1 - Promeni naziv.");
			System.out.println("2 - Promeni opis.");
			System.out.println("3 - Promeni kolicinu.");
			System.out.println("4 - Sacuvaj izmene.");
			System.out.println("0 - Nazad.");
			try {
				input = CustomScanner.nextInt("Vas izbor: ");
				checkInput(input, hEquipment);
			} catch (IOException | InvalidAttributeValueException | ResourceNotFound e) {
				System.out.println(e.getMessage());
			}
		} while (input != 0);
	}

	private void printEquipment(HospitalEquipment hEquipment) {
		System.out.println("ID: " + hEquipment.getId() + "\nIme: " + hEquipment.getName() + "\nOpis: "
				+ hEquipment.getDescription() + "\nUkupna kolicina: " + hEquipment.getTotalEquipment());
	}

	private void checkInput(int input, HospitalEquipment hEquipment) throws InvalidAttributeValueException, ResourceNotFound {
		switch (input) {
		case 0:
			break;
		case 1:
			updateEquName(hEquipment);
			break;
		case 2:
			updateEquDescription(hEquipment);
			break;
		case 3:
			updateTotalEqu(hEquipment);
			break;
		case 4:
			equipmentService.update(hEquipment);
			System.out.println("\nIzmene su sacuvane.\n");

			break;
		default:
			System.err.println("\nPogresan izbor.\n");
		}
	}

	public void updateTotalEqu(HospitalEquipment hEquipment) {
		int totalEqu = 0;
		try {
			totalEqu = CustomScanner.nextInt("Unesi kolicinu opreme: ");
			if (totalEqu < 0) {
				System.err.println("Pogresan unos.");
				return;
			}
		} catch (IOException e) {
			System.err.println(e.toString());
			return;
		}
		hEquipment.setTotalEquipment(totalEqu);
	}

	public void updateEquDescription(HospitalEquipment hEquipment) {
		String hEquDescription = CustomScanner.nextLine("Unesi opis: ");
		hEquipment.setDescription(hEquDescription);
	}

	public void updateEquName(HospitalEquipment hEquipment) {
		String newName = CustomScanner.nextLine("Unesi novo ime: ");
		hEquipment.setName(newName);
	}
}
