package menu.manager.update_room;

import java.io.IOException;

import javax.management.InvalidAttributeValueException;

import dto.date.DateDTO;
import dto.room.RoomDTO;
import exceptions.CustomException;
import exceptions.ResourceNotFound;
import factory.RoomServiceFactory;
import models.user.User;
import service.room.IRoomService;
import utility.CustomScanner;

public class UpdateRoomMenu {
	private static UpdateRoomMenu updateRoomMenu;

	private IRoomService roomService;

	private UpdateRoomMenu() {
		RoomServiceFactory roomServiceFactory = new RoomServiceFactory();
		this.roomService = roomServiceFactory.getRoomService();
	}

	public static UpdateRoomMenu getInstance() {
		if (updateRoomMenu == null) {
			updateRoomMenu = new UpdateRoomMenu();
		}
		return updateRoomMenu;
	}

	public void showMenu(User user) throws ResourceNotFound, CustomException{
		String roomId = CustomScanner.nextLine("Unesite ID sobe koju zelite urediti: ");
		RoomDTO room = roomService.getRoomWithEquipment(roomId);
		int input = 0;
		do {
			System.out.println(room.toString());
			System.out.println("1 - Izmena nemene prostorije.");
			System.out.println("2 - Premestanje opreme kojom prostorija raspolaze.");
			System.out.println("3 - Zakazi renoviranje.");
			System.out.println("0 - Nazad.");
			try {
				input = CustomScanner.nextInt("Vas izbor: ");
				checkInput(input, user, room);
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}while(input!=0 && input!=3);
	}

	private void checkInput(int input, User user, RoomDTO room) throws CustomException, ResourceNotFound {
		switch (input) {
		case 0:
			break;
		case 1:
			UpdateRoomTypeMenu updateRoomTypeMenu = UpdateRoomTypeMenu.getInstance();
			updateRoomTypeMenu.updateRoomTypeMenu(room.getRoom());
			break;
		case 2:
			UpdateRoomEquipmentMenu updateRoomEquipmentMenu = UpdateRoomEquipmentMenu.getInstance();
			updateRoomEquipmentMenu.updateRoomEquipment(room.getEquipmentList());
			break;
		case 3:
			addRenovation(room,user);
			break;
		default:
			System.out.println("\nPogresan izbor.\n");
		}
	}
	
	public void addRenovation(RoomDTO room, User user) {
		DateDTO date = new DateDTO();
		try {
			roomService.updateRoom(room, date, user);
			System.out.println("Zakazano je renoviranje sobe '"+room.getRoom().getId()+"'.");
		} catch (InvalidAttributeValueException | ResourceNotFound e) {
			System.err.println(e.getMessage());
		}
	}
}
