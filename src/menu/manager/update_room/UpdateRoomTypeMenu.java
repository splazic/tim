package menu.manager.update_room;

import java.io.IOException;

import exceptions.CustomException;
import factory.RoomServiceFactory;
import models.room.Room;
import models.room.RoomType;
import service.room.IRoomService;
import utility.CustomScanner;

public class UpdateRoomTypeMenu {
	private static UpdateRoomTypeMenu updateRoomTypeMenu = null;

	IRoomService roomService;

	private UpdateRoomTypeMenu() {
		RoomServiceFactory roomServiceFactory = new RoomServiceFactory();
		this.roomService = roomServiceFactory.getRoomService();
	}

	public static UpdateRoomTypeMenu getInstance() {
		if (updateRoomTypeMenu == null)
			updateRoomTypeMenu = new UpdateRoomTypeMenu();
		return updateRoomTypeMenu;
	}

	public void updateRoomTypeMenu(Room room) throws CustomException {
		System.out.println("Trenutna namena sobe: " + room.RoomTypeToString());
		System.out.println("Izaberi novu namenu sobe: ");
		System.out.println("1 - SALA ZA PREGLEDE");
		System.out.println("2 - OPERACIONA SALA");
		System.out.println("3 - SOBA ZA LEZANJE");
		try {
			int input = CustomScanner.nextInt("Vas izbor: ");
			checkInput(input, room);
			if (input >= 1 && input <= 3) {
				return;
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new CustomException("Pogresan unos.");
		}
	}

	private void checkInput(int input, Room room) {
		switch (input) {
		case 1:
			room.setRoomType(RoomType.EXAMINATION);
			break;
		case 2:
			room.setRoomType(RoomType.OPERATION);
			break;
		case 3:
			room.setRoomType(RoomType.REST);
			break;
		default: 
			System.out.println("\nPogresan izbor.\n");
		}
	}

}
