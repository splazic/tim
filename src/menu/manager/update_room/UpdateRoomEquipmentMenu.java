package menu.manager.update_room;

import java.io.IOException;
import java.util.List;

import dto.room.RoomEquDTO;
import exceptions.ResourceNotFound;
import factory.EquipmentServiceFactory;
import models.equipment.HospitalEquipment;
import service.equipment.IHospitalEquipmentService;
import utility.CustomScanner;

public class UpdateRoomEquipmentMenu {

	private static UpdateRoomEquipmentMenu updateRoomEquipment;

	private IHospitalEquipmentService equipmentService;

	private UpdateRoomEquipmentMenu() {
		EquipmentServiceFactory equipmentServiceFactory = new EquipmentServiceFactory();
		this.equipmentService = equipmentServiceFactory.getEquipmentService();
	}

	public static UpdateRoomEquipmentMenu getInstance() {
		if (updateRoomEquipment == null) {
			updateRoomEquipment = new UpdateRoomEquipmentMenu();
		}
		return updateRoomEquipment;
	}

	public void updateRoomEquipment(List<RoomEquDTO> roomEqu) throws ResourceNotFound {
		System.out.println("1 - Dodaj opremu.");
		System.out.println("2 - Izbaci opremu.");
		try {
			int input = CustomScanner.nextInt("Vas izbor: ");
			checkInput(input, roomEqu);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private void checkInput(int input, List<RoomEquDTO> roomEqu) throws ResourceNotFound, IOException {
		switch (input) {
		case 1:
			addEquipmentInRoom(roomEqu);
			break;
		case 2:
			removeEquipmentFromRoom(roomEqu);
			break;
		default: 
			System.out.println("\nPogresan izbor.\n");
		}
	}

	private void addEquipmentInRoom(List<RoomEquDTO> roomEqu) throws ResourceNotFound, IOException {
		String equName = CustomScanner.nextLine("Unesite ime opreme: ");
		HospitalEquipment equipment = equipmentService.getHospitalEquimpent(equName);
		int freeEqu = equipmentService.getFreeEquipment(equipment.getId());
		System.out.println("Oprema: " + equipment.getName() + ", ukupno: " + equipment.getTotalEquipment()+ ", slobodno: " + freeEqu);
		int quantity = CustomScanner.nextInt("Unesite kolicinu opreme: ");
		if(quantity<1) throw new ResourceNotFound("Greska. Kolicina oprema mora biti veca od 0.");
		for (int i = 0; i < roomEqu.size(); ++i) {
			int roomEquId = roomEqu.get(i).getEquipment().getId();
			if (roomEquId == (equipment.getId())) {
				if (roomEqu.get(i).getQuantity() < quantity) {
					if ((quantity - roomEqu.get(i).getQuantity()) > freeEqu) 
						throw new ResourceNotFound("Greska. Nema dovoljno opreme '" + equipment.getName() + "' na raspolaganju.");
				}
				roomEqu.set(i, new RoomEquDTO(equipment, quantity));
				return;
			}
		}
		if (quantity <= freeEqu) {
			roomEqu.add(new RoomEquDTO(equipment, quantity));
		}else
			throw new ResourceNotFound("Greska. Nema dovoljno opreme '" + equipment.getName() + "' na raspolaganju.");
	}

	private void removeEquipmentFromRoom(List<RoomEquDTO> roomEqu) throws ResourceNotFound, IOException {
		String equName = CustomScanner.nextLine("Unesite ime opreme koju zelite izbrisati: ");
		HospitalEquipment equipment = equipmentService.getHospitalEquimpent(equName);
		for (int i = 0; i < roomEqu.size(); ++i) {
			int roomEquId = roomEqu.get(i).getEquipment().getId();
			if (roomEquId == (equipment.getId())) {
				roomEqu.remove(i);
				System.out.println("Oprema je izbrisana.");
				return;
			}
		}
		throw new ResourceNotFound("Greska. Oprema '" + equipment.getName() + "' nije rezervisana.");
	}
}
