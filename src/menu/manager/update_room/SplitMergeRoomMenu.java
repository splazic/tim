package menu.manager.update_room;

import java.io.IOException;
import java.text.ParseException;

import dto.date.DateDTO;
import exceptions.CustomException;
import exceptions.ResourceNotFound;
import factory.RoomServiceFactory;
import models.room.Room;
import models.user.User;
import service.room.IRoomService;
import utility.CustomScanner;

public class SplitMergeRoomMenu {

	private static SplitMergeRoomMenu splitMergeRoomMenu;
	
	private IRoomService roomService;
	
	private SplitMergeRoomMenu() {
		RoomServiceFactory roomServiceFactory = new RoomServiceFactory();
		roomService = roomServiceFactory.getRoomService();
	}

	public static SplitMergeRoomMenu getInstance() {
		if (splitMergeRoomMenu == null) {
			splitMergeRoomMenu = new SplitMergeRoomMenu();
		}
		return splitMergeRoomMenu;
	}

	public void showMenu(User user) throws ResourceNotFound {
		Room room1 = roomService.getById(CustomScanner.nextLine("Unesite ID prve sobe: "));
		Room room2 = roomService.getById(CustomScanner.nextLine("Unesite ID druge sobe: "));
		DateDTO date = new DateDTO();
		try {
			date.setStartDate(CustomScanner.getDateTime("Unesi datum pocetka renoviranja."));
			date.setEndDate(CustomScanner.getDateTime("Unesi datum zavrsetka renoviranja."));
		}catch (ParseException e) {
			System.err.println(e.getMessage()); return;
		}
		if(room1.equals(room2)) {
			System.out.println("Greska. Unesite razlicite sobe."); return;
		}
		System.out.println("1 - Spajanje prostorija.");
		System.out.println("2 - Deljenje prostorija.");
		try {
			int input = CustomScanner.nextInt("Vas izbor: ");
			checkInput(input, user, room1, room2, date);
		} catch (CustomException | IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private void checkInput(int input, User user,Room room1, Room room2, DateDTO date) throws ResourceNotFound, IOException, CustomException {
		switch (input) {
		case 1:
			mergeRooms(user, room1, room2, date);
			break;
		case 2:
			splitRoom(user, room1, room2, date);
			break;
		default: 
			System.out.println("\nPogresan izbor.\n");
		}
	}
	
	public void mergeRooms(User user, Room room1, Room room2, DateDTO date) throws ResourceNotFound, CustomException {
		roomService.mergeRooms(user, room1, room2, date);
	}

	public void splitRoom(User user, Room room1, Room room2, DateDTO date) throws ResourceNotFound, CustomException  {
		roomService.splitRoom(user, room1, room2, date);
	}
}
