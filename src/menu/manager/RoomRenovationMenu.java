package menu.manager;

import java.io.IOException;

import exceptions.CustomException;
import exceptions.ResourceNotFound;
import menu.manager.update_room.SplitMergeRoomMenu;
import menu.manager.update_room.UpdateRoomMenu;
import models.user.User;
import utility.CustomScanner;

class RoomRenovationMenu {
	private static RoomRenovationMenu roomRenovationMenu = null;

	private RoomRenovationMenu() {
	}

	public static RoomRenovationMenu getInstance() {
		if (roomRenovationMenu == null)
			roomRenovationMenu = new RoomRenovationMenu();
		return roomRenovationMenu;
	}

	public void showMenu(User user) {
		int input = 0;
		do {
			System.out.println("1 - Uredi prostoriju.");
			System.out.println("2 - Deljenje / spajanje prostorija.");
			System.out.println("3 - Ucini prostoriju nedostupnom na odredjeni period.");
			System.out.println("0 - Nazad.");
			try {
				input = CustomScanner.nextInt("Vas izbor: ");
				checkInput(input, user);
			} catch (ResourceNotFound | IOException | CustomException e) {
				System.err.println(e.getMessage());
			}
		} while (input != 0);
	}

	private void checkInput(int input, User user) throws CustomException, ResourceNotFound {
		switch (input) {
		case 0:
			break;
		case 1:
			UpdateRoomMenu updateRoomMenu = UpdateRoomMenu.getInstance();
			updateRoomMenu.showMenu(user);
			break;
		case 2:
			SplitMergeRoomMenu splitMergeRoomMenu = SplitMergeRoomMenu.getInstance();
			splitMergeRoomMenu.showMenu(user);
			break;
		case 3:
			//makeRoomUnavailble
			break;
		default:
			System.out.println("\nPogresan izbor.\n");
		}
	}

}
