package menu.menu;

import javax.security.auth.login.CredentialNotFoundException;

import factory.UserServiceFactory;
import menu.administrator.AdministratorMenu;
import menu.doctor.DoctorMenu;
import menu.manager.ManagerMenu;
import menu.patient.PatientMenu;
import menu.secretary.SecretaryMenu;
import models.user.Administrator;
import models.user.Manager;
import models.user.Secretary;
import models.user.User;
import models.user.doctor.Doctor;
import models.user.patient.Patient;
import service.user.IUserService;
import utility.CustomScanner;
import utility.FileManager;

public class Menu {
	private static Menu menu = null;

	private IUserService userService;

	private Menu() {
		UserServiceFactory userServiceFactory = new UserServiceFactory();
		userService = userServiceFactory.getUserService();
	}

	public static Menu getInstance() {
		if (menu == null)
			menu = new Menu();
		return menu;
	}

	public void startMenu() {
		int choice = 0;
		do {
			System.out.println("1 - Prijavi se.");
			System.out.println("0 - Zatvori aplikaciju.");
			try {
				choice = CustomScanner.nextInt("Vas izbor: ");
			if (choice == 1)
				loginMenu();
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
			if(choice!=0 && choice!=1)
				System.err.println("Pogresan izbor.");
		} while (choice != 0);
	}

	private void loginMenu() {
		String userName = CustomScanner.nextLine("Unesi korinsicko ime: ");
		String password = CustomScanner.nextLine("Unesi lozinku: ");
		try {
			User user = userService.checkCredentials(userName, password);
			FileManager.saveLogedUserData(user);
			showMenu(user);
		} catch (CredentialNotFoundException e) {
			System.err.println(e.getMessage());
		}
	}

	private void showMenu(User user) {
		if (user instanceof Administrator) {
			AdministratorMenu administratorMenu = AdministratorMenu.getInstance();
			administratorMenu.showMenu(user);
		} else if (user instanceof Secretary) {
			SecretaryMenu secretaryMenu = SecretaryMenu.getInstance();
			secretaryMenu.showMenu(user);
		} else if (user instanceof Manager) {
			ManagerMenu managerMenu = ManagerMenu.getInstance();
			managerMenu.showMenu(user);
		} else if (user instanceof Doctor) {
			DoctorMenu doctorMenu = DoctorMenu.getInstance();
			doctorMenu.showMenu(user);
		} else if (user instanceof Patient) {
			PatientMenu patientMenu = PatientMenu.getInstance();
			patientMenu.showMenu(user);
		} else {
			System.out.println(user.getUsername());
		}
	}
}
