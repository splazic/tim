/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.secretary;

import java.util.Date;

import models.medical_record.MedicalRecord;
import models.user.patient.Patient;
import repository.medical_record.medical_rec_item.MedicalRecItemFileRepo;
import repository.medical_record.medical_record.MedicalRecordFileRepo;
import repository.user.patient.PatientFileRepo;
import repository.user.user.UserFileRepo;
import service.medical_record.IMedicalRecordService;
import service.medical_record.MedicalRecordItemService;
import service.medical_record.MedicalRecordService;
import service.user.IPatientService;
import service.user.IUserService;
import service.user.PatientService;
import service.user.UserService;
import utility.CustomScanner;
import utility.FileManager;

/**
 *
 * @author stefan
 */
public class InsertPatientMenu {

    public static InsertPatientMenu insertPatientMenu = null;
    private IMedicalRecordService medialRecordService = new MedicalRecordService(new MedicalRecordFileRepo(FileManager.MEDICALRECORD_URI), new MedicalRecordItemService(new MedicalRecItemFileRepo(FileManager.MEDICAL_RECORD_ITEM_URI)));
    private IUserService userService = new UserService((new UserFileRepo(FileManager.USER_URI)));
    private IPatientService patientService = new PatientService(new PatientFileRepo(FileManager.USER_URI));

    private InsertPatientMenu() {
    }

    public static InsertPatientMenu getInstance() {
        if (insertPatientMenu == null) {
            insertPatientMenu = new InsertPatientMenu();

        }
        return insertPatientMenu;
    }

    public void insertPatient() {
        try {
            MedicalRecord record = insertRecord();
            String username = getUsername();
            userService.getByUsername(username);
            String medialNumber = getMedicalNumber();
            Date birthDate = CustomScanner.getDate("Unesite datum formata dd-mm-yyyy");
            String password = getPassword();
            String fName = getFirstName();
            String lName = getLastName();
            boolean isMale = CustomScanner.getBoolean("Is it male");
            Patient patient = new Patient(medialNumber, isMale, record.getId(), birthDate, username, password, fName, lName);
            //save patient to the list
            patientService.save(patient);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    private MedicalRecord insertRecord() throws Exception {
        float height = CustomScanner.getFloat("Unesi visinu");
        float weight = CustomScanner.getFloat("Unesi tezinu");
        MedicalRecord record = new MedicalRecord(height, weight);
        medialRecordService.save(record);
        return record;

    }

    private String getMedicalNumber() {
        String medicalNumber = CustomScanner.nextLine("JMBG");
        return medicalNumber;
    }

    private String getUsername() {
        String username = CustomScanner.nextLine("Korisnicko ime");
        return username;
    }

    private String getPassword() {
        String password = CustomScanner.nextLine("Lozinka");
        return password;
    }

    private String getLastName() {
        String lastName = CustomScanner.nextLine("Prezime");
        return lastName;
    }

    private String getFirstName() {
        String firstName = CustomScanner.nextLine("Ime");
        return firstName;
    }
}
