/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.secretary;


import java.util.logging.Level;
import java.util.logging.Logger;
import models.user.User;
import utility.CustomScanner;

/**
 *
 * @author stefan
 */
public class SecretaryMenu {

    private static SecretaryMenu secretaryMenu = null;
    private static InsertPatientMenu insertPatientMenu = null;

    public static SecretaryMenu getInstance() {
        if (secretaryMenu == null) {
            secretaryMenu = new SecretaryMenu();
        }
        return secretaryMenu;
    }

    public void showMenu(User user) {
        int input = 0;
        do {
            try {
                System.out.println("1 - Unesi pacijenta");
                System.out.println("0 - Odjavi se");
                input = CustomScanner.nextInt("Vas izbor: ");
                checkInput(input);
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        } while (input != 0);
    }

    private static void checkInput(int input) {
        switch (input) {
            case 0:
                break;
            case 1:
                insertPatientMenu = InsertPatientMenu.getInstance();
                insertPatientMenu.insertPatient();
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            default:
                System.out.println("\nPogresan izbor.\n");
        }
    }
}
