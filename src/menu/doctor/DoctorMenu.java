package menu.doctor;

import models.user.User;
import models.user.doctor.Doctor;
import utility.CustomScanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author stefan
 */
public class DoctorMenu {

    private static DoctorMenu doctorMenu = null;

    private DoctorMenu() {
    }

    public static DoctorMenu getInstance() {
        if (doctorMenu == null) {
            doctorMenu = new DoctorMenu();
        }
        return doctorMenu;
    }

    public void showMenu(User user) {
        int input = 0;
        Doctor doctor = (Doctor) user;
        do {
            System.out.println("1 - Pregled svojih zauzeca");
            System.out.println("2 - Azuriraj anamnezu");
            System.out.println("3 - Generisi izvestaj");
            System.out.println("4 - Zakazi");
            System.out.println("0 - Odjavi se.");
            try {
                input = CustomScanner.nextInt("Vas izbor: ");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            checkInput(input, doctor);
        } while (input != 0);
    }

    private void checkInput(int input, Doctor doctor) {
        DoctorAppointmentMenu doctorAppointmentMenu;
        switch (input) {
            case 1:
                doctorAppointmentMenu = DoctorAppointmentMenu.getInstance();
                doctorAppointmentMenu.showAppointments(doctor);
                break;
            case 2:
                doctorAppointmentMenu = DoctorAppointmentMenu.getInstance();
                doctorAppointmentMenu.updateMedicalRecord(doctor);
                break;
            case 3:
                GenerateReportMenu generateReportMenu = GenerateReportMenu.getInstance();
                generateReportMenu.generateReport(doctor);
                break;
            case 4:
                doctorAppointmentMenu = DoctorAppointmentMenu.getInstance();
                doctorAppointmentMenu.schedule(doctor);
                break;
            default:
                System.out.println("\nPogresan izbor.\n");
        }
    }
}
