/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.doctor;

import dto.appointment.AppointmentDTO;
import exceptions.CustomException;
import exceptions.ResourceNotFound;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.appointment.TimeSlot;
import models.medical_record.MedicalRecordItem;
import models.user.User;
import models.user.doctor.Doctor;
import models.user.doctor.Specialisation;
import models.user.patient.Patient;
import repository.appointment.appointment.AppointmentFileRepo;
import repository.appointment.appointment_time_slot.AppointmentTimeSlotFileRepo;
import repository.appointment.appointment_type.MedicalAppointmentTypeFileRepo;
import repository.appointment.time_slot.TimeSlotFileRepo;
import repository.medical_record.medical_rec_item.MedicalRecItemFileRepo;
import repository.notification.NotificationFileRepo;
import repository.room.room.RoomFileRepo;
import repository.user.doctor.DoctorSpecialisationFileRepo;
import repository.user.doctor.SpecialisationFileRepo;
import repository.user.user.UserFileRepo;
import service.appointment.AppointmentService;
import service.appointment.IAppointmentService;
import service.medical_record.IMedicalRecordItemService;
import service.medical_record.MedicalRecordItemService;
import service.specialisation.DoctorSpecialisationService;
import service.specialisation.IDoctorSpecialisationService;
import service.user.IUserService;
import service.user.UserService;
import utility.CustomScanner;
import utility.FileManager;

/**
 *
 * @author stefan
 */
public class DoctorAppointmentMenu {
    
    public static DoctorAppointmentMenu doctorAppointmentMenu = null;
    IAppointmentService appointmentService = new AppointmentService(new AppointmentFileRepo(FileManager.APPOINTMENT_URI), new UserFileRepo(FileManager.USER_URI), new RoomFileRepo(FileManager.ROOM_URI), new MedicalAppointmentTypeFileRepo(FileManager.MEDICAL_APPOINTMENT_URI), new TimeSlotFileRepo(FileManager.TIME_SLOT_URI), new AppointmentTimeSlotFileRepo(FileManager.APPOINTMENT_TIMESLOT_URI), new NotificationFileRepo(FileManager.NOTIFICATION_URI));
    private IUserService userService = new UserService(new UserFileRepo(FileManager.USER_URI));
    private IMedicalRecordItemService recordItemService = new MedicalRecordItemService(new MedicalRecItemFileRepo(FileManager.MEDICAL_RECORD_ITEM_URI));
    private IDoctorSpecialisationService doctorSpecialisationService = new DoctorSpecialisationService(new DoctorSpecialisationFileRepo(FileManager.DOCTOR_SPECIALISATION_URI), new SpecialisationFileRepo(FileManager.SPECIALISATION_URI), new UserFileRepo(FileManager.USER_URI));
    
    private DoctorAppointmentMenu() {
    }
    
    public static DoctorAppointmentMenu getInstance() {
        if (doctorAppointmentMenu == null) {
            doctorAppointmentMenu = new DoctorAppointmentMenu();
        }
        return doctorAppointmentMenu;
    }
    
    public void showAppointments(Doctor doctor) {
        try {
            List<AppointmentDTO> appointmentDTOs = appointmentService.getActiveByDoctor(doctor.getId());
            for (AppointmentDTO adto : appointmentDTOs) {
                singleAppointment(adto);
            }
            if (appointmentDTOs.isEmpty()) {
                System.out.println("Nema zakazanih pregleda");
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    public void updateMedicalRecord(Doctor doctor) {
        try {
            //update user by id
            String username = CustomScanner.nextLine("Unesite korisnicko ime pacijenta: ");
            Patient patient = getPatientByUsername(username);
            String description = CustomScanner.nextLine("Unesite opis: ");
            MedicalRecordItem medicalRecordItem = new MedicalRecordItem(patient.getMedicalRecordId(), new Date(), doctor.getId(), description);
            recordItemService.save(medicalRecordItem);
            System.out.println("Opis uspesno unet.");
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    
    public void schedule(Doctor doctor) {
        try {
            List<Specialisation> specialisations = doctorSpecialisationService.getByDoctor(doctor.getId());
            if (specialisations.size() > 0) {
                scheduleOperation(doctor);
            } else {
                scheduleCheckout(doctor);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    private void scheduleOperation(Doctor doctor) {
        try {
            String username = CustomScanner.nextLine("Unesite korisnicko ime pacijenta: ");
            Patient patient = getPatientByUsername(username);
            Date date = CustomScanner.getDate("Datum operacije: ");
            LocalTime startTime = CustomScanner.getLocalTime("Vreme operacije: ");
            LocalTime endTime = startTime.plusMinutes(30);
            TimeSlot slot = new TimeSlot(startTime, endTime);
            String roomId = CustomScanner.nextLine("Broj sobe: ");
            boolean isUrgent = CustomScanner.getBoolean("Is it urgent?: ");
            appointmentService.createOperation(slot, date, roomId, patient, doctor, isUrgent);
            System.out.println("Izvrseno zakazivanje operacije");
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    private void scheduleCheckout(Doctor doctor) {
        try {
            Patient patient = getPatientByUsername(CustomScanner.nextLine("Unesite korisnicko ime pacijenta: "));
            Doctor doc = getDoctorByUsername(CustomScanner.nextLine("Unesite korisnicko ime doktora specijaliste: "));
            Date startDate = CustomScanner.getDate("Unesite pocetni datum: ");
            Date endDate = CustomScanner.getDate("Unesite krajnji datum: ");
            appointmentService.scheduleAppointment(doc, patient, startDate, endDate);
            System.out.println("Poslat zahtev za pregled");
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    private Patient getPatientByUsername(String username) throws ResourceNotFound, CustomException {
        User user = userService.getByUsername(username);
        if (!(user instanceof Patient)) {
            throw new CustomException("Ovaj korisnik nije pacijent!");
        }
        return (Patient) user;
    }
    
    private Doctor getDoctorByUsername(String username) throws ResourceNotFound, CustomException {
        User user = userService.getByUsername(username);
        if (!(user instanceof Doctor)) {
            throw new CustomException("Ovaj korisnik nije doctor!");
        }
        return (Doctor) user;
    }
    
    private void singleAppointment(AppointmentDTO appointmentDTO) {
        System.out.println("Datum: " + appointmentDTO.getDate());
        System.out.println("Soba: " + appointmentDTO.getRoomId());
        System.out.println(appointmentDTO.getPatient().toString());
    }
    
}
