/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.doctor;

import dto.report.DoctorScheduleDTO;
import dto.user.UserDTO;
import java.util.ArrayList;
import java.util.List;
import models.user.doctor.Doctor;
import repository.appointment.appointment.AppointmentFileRepo;
import repository.appointment.appointment_time_slot.AppointmentTimeSlotFileRepo;
import repository.appointment.appointment_type.MedicalAppointmentTypeFileRepo;
import repository.appointment.time_slot.TimeSlotFileRepo;
import repository.notification.NotificationFileRepo;
import repository.room.room.RoomFileRepo;
import repository.user.user.UserFileRepo;
import service.appointment.AppointmentService;
import service.appointment.IAppointmentService;
import utility.FileGenerator;
import utility.FileManager;

/**
 *
 * @author stefan
 */
public class GenerateReportMenu {

    public static GenerateReportMenu generateReportMenu = null;
    IAppointmentService appointmentService = new AppointmentService(new AppointmentFileRepo(FileManager.APPOINTMENT_URI), new UserFileRepo(FileManager.USER_URI), new RoomFileRepo(FileManager.ROOM_URI), new MedicalAppointmentTypeFileRepo(FileManager.MEDICAL_APPOINTMENT_URI), new TimeSlotFileRepo(FileManager.TIME_SLOT_URI), new AppointmentTimeSlotFileRepo(FileManager.APPOINTMENT_TIMESLOT_URI), new NotificationFileRepo(FileManager.NOTIFICATION_URI));

    private GenerateReportMenu() {
    }

    public static GenerateReportMenu getInstance() {
        if (generateReportMenu == null) {
            generateReportMenu = new GenerateReportMenu();
        }
        return generateReportMenu;
    }

    public void generateReport(Doctor doctor) {
        try {
            DoctorScheduleDTO doctorScheduleDTO = appointmentService.getDoctorReport(doctor.getId());
            List<Object> patientsReport = appointmentService.getPatientsByDoctor(doctor.getId());
            List<String> colums = generateReportColumns();
            List<String> patientColumns = generatePatientColumns();
            List<Object> data = new ArrayList<>();

            data.add(doctorScheduleDTO);

            FileGenerator.generatePdf(colums, data, FileGenerator.generateFileName() + "doctor.pdf");
            FileGenerator.generateCSV(colums, data, FileGenerator.generateFileName() + "doctor.csv");

            FileGenerator.generatePdf(patientColumns, patientsReport, FileGenerator.generateFileName() + "patient.pdf");
            FileGenerator.generateCSV(patientColumns, patientsReport, FileGenerator.generateFileName() + "patient.csv");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    private List<String> generateReportColumns() {
        List<String> colums = new ArrayList<>();

        colums.add("Zauzece lekara");
        return colums;
    }

    private List<String> generatePatientColumns() {
        List<String> patientColumns = new ArrayList<>();

        patientColumns.add("firstName");
        patientColumns.add("lastName");
        patientColumns.add("username");
        return patientColumns;
    }
}
