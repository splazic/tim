package dto.renovation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.renovation.Renovation;
import models.renovation.RenovationRoom;
import models.renovation.RenovationType;

public class RenovationDTO {
	private int id;
	private int mangerId;
	private Date renovationStartDate;
	private Date renovatinoEndDate;
	private RenovationType renovationType;
	private List<String> rooms;

	private Renovation renovation;
	private List<RenovationRoom> renRoomList;

	public RenovationDTO(Renovation renovation, List<RenovationRoom> renRoomList) {
		this.id = renovation.getId();
		this.mangerId = renovation.getMangerId();
		this.renovatinoEndDate = renovation.getRenovatinoEndDate();
		this.renovationStartDate = renovation.getRenovationStartDate();
		this.renovationType = renovation.getRenovationType();
		rooms = new ArrayList<String>();
		for (RenovationRoom renovationRoom : renRoomList) {
			rooms.add(renovationRoom.getRoomId());
		}
	}

	public RenovationDTO(int id, int mangerId, Date renovationStartDate, Date renovatinoEndDate,
			RenovationType renovationType, List<String> rooms, Renovation renovation,
			List<RenovationRoom> renRoomList) {
		super();
		this.id = id;
		this.mangerId = mangerId;
		this.renovationStartDate = renovationStartDate;
		this.renovatinoEndDate = renovatinoEndDate;
		this.renovationType = renovationType;
		this.rooms = rooms;
		this.renovation = renovation;
		this.renRoomList = renRoomList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMangerId() {
		return mangerId;
	}

	public void setMangerId(int mangerId) {
		this.mangerId = mangerId;
	}

	public Date getRenovationStartDate() {
		return renovationStartDate;
	}

	public void setRenovationStartDate(Date renovationStartDate) {
		this.renovationStartDate = renovationStartDate;
	}

	public Date getRenovatinoEndDate() {
		return renovatinoEndDate;
	}

	public void setRenovatinoEndDate(Date renovatinoEndDate) {
		this.renovatinoEndDate = renovatinoEndDate;
	}

	public RenovationType getRenovationType() {
		return renovationType;
	}

	public void setRenovationType(RenovationType renovationType) {
		this.renovationType = renovationType;
	}

	public List<String> getRooms() {
		return rooms;
	}

	public void setRooms(List<String> rooms) {
		this.rooms = rooms;
	}

	public Renovation getRenovation() {
		return renovation;
	}

	public void setRenovation(Renovation renovation) {
		this.renovation = renovation;
	}

	public List<RenovationRoom> getRenRoomList() {
		return renRoomList;
	}

	public void setRenRoomList(List<RenovationRoom> renRoomList) {
		this.renRoomList = renRoomList;
	}

}
