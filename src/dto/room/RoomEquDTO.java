package dto.room;

import models.equipment.HospitalEquipment;

public class RoomEquDTO {
	private HospitalEquipment equipment;
	private int quantity;

	public RoomEquDTO(HospitalEquipment equipment, int quantity) {
		super();
		this.equipment = equipment;
		this.quantity = quantity;
	}

	public HospitalEquipment getEquipment() {
		return equipment;
	}

	public void setEquipment(HospitalEquipment equipment) {
		this.equipment = equipment;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Naziv opreme: " + equipment.getName() + ", kolicina: " + quantity + "\n"; 
	}

}
