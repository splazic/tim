package dto.room;

import java.util.List;

import models.room.Room;

public class RoomDTO {
	private Room room;
	private List<RoomEquDTO> equipmentList;

	public RoomDTO() {
		// TODO Auto-generated constructor stub
	}

	public RoomDTO(Room room, List<RoomEquDTO> equipmentList) {
		super();
		this.room = room;
		this.equipmentList = equipmentList;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public List<RoomEquDTO> getEquipmentList() {
		return equipmentList;
	}

	public void setEquipmentList(List<RoomEquDTO> equipmentList) {
		this.equipmentList = equipmentList;
	}

	@Override
	public String toString() {
		return "Soba: "+room.getId() + ", namena: " + room.getRoomType() + "\n" + equipmentListToString();
	}
	
	private String equipmentListToString() {
		StringBuilder str = new StringBuilder();
		for (RoomEquDTO roomEquDTO : equipmentList) {
			str.append(roomEquDTO.toString());
		}
		return str.toString();
	}

}
