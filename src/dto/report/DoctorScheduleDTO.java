/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.report;

/**
 *
 * @author stefan
 */
public class DoctorScheduleDTO {

    private float totalScheduleHours;

    public DoctorScheduleDTO() {
    }

    public DoctorScheduleDTO(float totalWorkHOurs) {
        this.totalScheduleHours = totalWorkHOurs;
    }

    public float getTotalScheduleHours() {
        return totalScheduleHours;
    }

    public void setTotalScheduleHours(float totalScheduleHours) {
        this.totalScheduleHours = totalScheduleHours;
    }

    @Override
    public String toString() {
        return "" + totalScheduleHours;
    }

}
