package dto.medical_record;

import java.util.List;

import models.medical_record.MedicalRecord;
import models.medical_record.MedicalRecordItem;

public class MedicalRecordDTO {
	private MedicalRecord medicalRecord;
	private List<MedicalRecordItem> medRecordItems;

	public MedicalRecordDTO(MedicalRecord medicalRecord, List<MedicalRecordItem> medRecordItems) {
		super();
		this.medicalRecord = medicalRecord;
		this.medRecordItems = medRecordItems;
	}

	public MedicalRecord getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(MedicalRecord medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	public List<MedicalRecordItem> getMedRecordItems() {
		return medRecordItems;
	}

	public void setMedRecordItems(List<MedicalRecordItem> medRecordItems) {
		this.medRecordItems = medRecordItems;
	}

}
