/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.appointment;

import java.time.LocalTime;
import models.appointment.TimeSlot;

/**
 *
 * @author stefan
 */
public class TimeSlotDTO {

    private LocalTime startTime;
    private LocalTime endTime;

    public TimeSlotDTO() {
    }

    public TimeSlotDTO(TimeSlot timeSlot) {
        this.startTime = timeSlot.getStartTime();
        this.endTime = timeSlot.getEndTime();
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "TimeSlotDTO{" + "startTime=" + startTime + ", endTime=" + endTime + '}';
    }

}
