/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.appointment;

import dto.user.UserDTO;
import java.time.LocalDate;
import models.appointment.Appointment;
import models.appointment.AppointmentStatus;
import models.appointment.type.MedicalAppointmentType;
import models.user.User;

/**
 *
 * @author stefan
 */
public class AppointmentDTO {

    private int id;
    private UserDTO doctor;
    private UserDTO patient;
    private String roomId;
    private LocalDate date;
    private AppointmentStatus appointmentStatus;
    private MedicalAppointmentTypeDTO appointmentType;

    public AppointmentDTO() {
    }

    public AppointmentDTO(Appointment appointment, User user, User patient, MedicalAppointmentType appointmentType) {
        this.roomId = appointment.getRoomId();
        this.date = appointment.getDate();
        this.appointmentStatus = appointment.getAppointmentStatus();
        this.doctor = new UserDTO(user);
        this.appointmentType = new MedicalAppointmentTypeDTO(appointmentType);
        this.id = appointment.getId();
        this.patient = this.patient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserDTO getDoctor() {
        return doctor;
    }

    public void setDoctor(UserDTO doctor) {
        this.doctor = doctor;
    }

    public UserDTO getPatient() {
        return patient;
    }

    public void setPatient(UserDTO patient) {
        this.patient = patient;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public AppointmentStatus getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public MedicalAppointmentTypeDTO getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(MedicalAppointmentTypeDTO appointmentType) {
        this.appointmentType = appointmentType;
    }

    @Override
    public String toString() {
        return "AppointmentDTO{" + "id=" + id + ", doctor=" + doctor + ", patient=" + patient + ", roomId=" + roomId + ", date=" + date + ", appointmentStatus=" + appointmentStatus + ", appointmentType=" + appointmentType + '}';
    }

}
