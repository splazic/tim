/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.appointment;

import models.appointment.type.AppointmentType;
import models.appointment.type.MedicalAppointmentType;

/**
 *
 * @author stefan
 */
public class MedicalAppointmentTypeDTO {

    private String name;
    private String description;
    private AppointmentType appointmentType;

    public MedicalAppointmentTypeDTO() {
    }

    public MedicalAppointmentTypeDTO(MedicalAppointmentType medicalAppointmentType) {
        this.name = medicalAppointmentType.getName();
        this.description = medicalAppointmentType.getDescription();
        this.appointmentType = medicalAppointmentType.getAppointmentType();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AppointmentType getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(AppointmentType appointmentType) {
        this.appointmentType = appointmentType;
    }

    @Override
    public String toString() {
        return "MedicalAppointmentTypeDTO{" + "name=" + name + ", description=" + description + ", appointmentType=" + appointmentType + '}';
    }

}
