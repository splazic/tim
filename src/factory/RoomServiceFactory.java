package factory;

import repository.appointment.appointment.AppointmentFileRepo;
import repository.appointment.appointment.AppointmentRepository;
import repository.hospital_equipment.equipment.HospitalEquipmentFileRepo;
import repository.hospital_equipment.equipment.HospitalEquipmentRepository;
import repository.renovation.renovation_room.RenovationRoomFileRepo;
import repository.renovation.renovation_room.RenovationRoomRepository;
import repository.room.room.RoomFileRepo;
import repository.room.room.RoomRepository;
import repository.room.room_equipment.RoomEquipmentFileRepo;
import repository.room.room_equipment.RoomEquipmentRepository;
import service.room.IRoomService;
import service.room.RoomService;
import utility.FileManager;

public class RoomServiceFactory {
	
	public IRoomService getRoomService() {
		
		RoomRepository roomRepo = new RoomFileRepo(FileManager.ROOM_URI);
		HospitalEquipmentRepository equRepo = new HospitalEquipmentFileRepo(FileManager.HOSPITAL_EQUIPMENT_URI);
		RoomEquipmentRepository roomEquRepo = new RoomEquipmentFileRepo(FileManager.ROOM_EQUIPMENT_URI);
		AppointmentRepository appointmentRepo = new AppointmentFileRepo(FileManager.APPOINTMENT_URI);
		RenovationRoomRepository renovationRoomRepo = new RenovationRoomFileRepo(FileManager.ROOM_RENOVATION_URI);
		
		RoomService roomService = new RoomService(roomRepo, roomEquRepo, equRepo, appointmentRepo, renovationRoomRepo);
		return roomService;
	}
}
