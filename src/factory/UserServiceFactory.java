package factory;

import repository.user.user.UserFileRepo;
import service.user.IUserService;
import service.user.UserService;
import utility.FileManager;

public class UserServiceFactory {
	public IUserService getUserService() {
		UserService userService = new UserService(new UserFileRepo(FileManager.USER_URI));
		return userService;
	}
}
