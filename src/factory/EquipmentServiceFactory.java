package factory;

import repository.appointment.appointment.AppointmentFileRepo;
import repository.appointment.appointment.AppointmentRepository;
import repository.hospital_equipment.equipment.HospitalEquipmentFileRepo;
import repository.hospital_equipment.equipment.HospitalEquipmentRepository;
import repository.hospital_equipment.required_equipment.RequiredEquFileRepo;
import repository.hospital_equipment.required_equipment.RequiredEquipmentRepository;
import repository.room.room_equipment.RoomEquipmentFileRepo;
import repository.room.room_equipment.RoomEquipmentRepository;
import service.equipment.HospitalEquipmentService;
import service.equipment.IHospitalEquipmentService;
import utility.FileManager;

public class EquipmentServiceFactory {
	public IHospitalEquipmentService getEquipmentService() {
		HospitalEquipmentRepository hospitalEquipmentRepo = new HospitalEquipmentFileRepo(
				FileManager.HOSPITAL_EQUIPMENT_URI);
		AppointmentRepository appointmentRepo = new AppointmentFileRepo(FileManager.APPOINTMENT_URI);
		RoomEquipmentRepository roomEquipmentRepo = new RoomEquipmentFileRepo(FileManager.ROOM_EQUIPMENT_URI);
		RequiredEquipmentRepository requiredEquipmentRepo = new RequiredEquFileRepo(FileManager.REQUIRED_EQUIPMENT_URI);
		
		HospitalEquipmentService hospitalEquipmentService = new HospitalEquipmentService(hospitalEquipmentRepo,
				roomEquipmentRepo, appointmentRepo, requiredEquipmentRepo);

		return hospitalEquipmentService;
	}
}
