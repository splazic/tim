package factory;

import repository.renovation.renovation.RenovationFileRepo;
import repository.renovation.renovation.RenovationRepository;
import service.renovation.IRenovationService;
import service.renovation.RenovationService;
import utility.FileManager;

public class RenovationServiceFactory {
	public IRenovationService getRenovationService() {
		RenovationRepository renovationRepo = new RenovationFileRepo(FileManager.RENOVATION_URI);
	
		IRenovationService renovationService = new RenovationService(renovationRepo);
	
		return renovationService;
	}
}
