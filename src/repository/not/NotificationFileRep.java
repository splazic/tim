/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.not;

import models.notification.Notification;
import repository.generic.GenericFileRepo;

/**
 *
 * @author stefan
 */
public class NotificationFileRep extends GenericFileRepo<Notification, Integer> implements NotificationRepository {

    public NotificationFileRep(String filePath) {
        super(filePath);
    }

}
