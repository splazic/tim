package repository.renovation.renovation;

import java.util.List;

import models.renovation.Renovation;
import repository.generic.GenericRepository;

public interface RenovationRepository extends GenericRepository<Renovation, Integer>{
	public List<Renovation> getScheduledRenovations();
}
