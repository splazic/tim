package repository.renovation.renovation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.renovation.Renovation;
import repository.generic.GenericFileRepo;
import utility.DateValidation;

public class RenovationFileRepo extends GenericFileRepo<Renovation, Integer> implements RenovationRepository{

	DateValidation validation;
	
	public RenovationFileRepo(String filePath) {
		super(filePath);
	}

	@Override
	public List<Renovation> getScheduledRenovations(){
		List<Renovation> list = new ArrayList<Renovation>();
		Date currentDate = new Date();
		for (Renovation renovation : getAll()) {
			Date startD = renovation.getRenovationStartDate();
			Date endD = renovation.getRenovatinoEndDate();
			if(validation.isDateOverlap(startD, endD) || startD.after(currentDate)) {
				list.add(renovation);
			}
		}
		return list;
	}
}
