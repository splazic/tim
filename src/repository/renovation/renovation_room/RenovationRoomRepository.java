package repository.renovation.renovation_room;

import java.util.List;

import models.renovation.RenovationRoom;
import repository.generic.GenericRepository;

public interface RenovationRoomRepository extends GenericRepository<RenovationRoom, Integer>{
	public List<RenovationRoom> getAll(int renovationId);
}
