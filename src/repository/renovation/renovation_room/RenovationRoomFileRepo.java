package repository.renovation.renovation_room;

import java.util.ArrayList;
import java.util.List;

import models.renovation.RenovationRoom;
import repository.generic.GenericFileRepo;

public class RenovationRoomFileRepo extends GenericFileRepo<RenovationRoom, Integer> implements RenovationRoomRepository{

	public RenovationRoomFileRepo(String filePath) {
		super(filePath);
	}
	
	@Override
	public List<RenovationRoom> getAll(int renovationId) {
		List<RenovationRoom> list = new ArrayList<RenovationRoom>();
		for (RenovationRoom renovationRoom : list) {
			if(renovationRoom.getRenovationId() == renovationId)
				list.add(renovationRoom);
		}
		return list;
	}

}
