package repository.medical_record.medical_rec_item;

import models.medical_record.MedicalRecordItem;
import repository.generic.GenericFileRepo;

public class MedicalRecItemFileRepo extends GenericFileRepo<MedicalRecordItem, Integer> implements MedicalRecItemRepository{

	public MedicalRecItemFileRepo(String filePath) {
		super(filePath);
	}

}
