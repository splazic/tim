package repository.medical_record.medical_rec_item;

import models.medical_record.MedicalRecordItem;
import repository.generic.GenericRepository;

public interface MedicalRecItemRepository extends GenericRepository<MedicalRecordItem, Integer>{

}
