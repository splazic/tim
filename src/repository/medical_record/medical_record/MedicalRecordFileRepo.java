package repository.medical_record.medical_record;

import models.medical_record.MedicalRecord;
import repository.generic.GenericFileRepo;

public class MedicalRecordFileRepo extends GenericFileRepo<MedicalRecord, Integer> implements MedicalRecordRepository{

	public MedicalRecordFileRepo(String filePath) {
		super(filePath);
	}

}
