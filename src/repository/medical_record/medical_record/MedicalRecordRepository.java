package repository.medical_record.medical_record;

import models.medical_record.MedicalRecord;
import repository.generic.GenericRepository;

public interface MedicalRecordRepository extends GenericRepository<MedicalRecord, Integer>{

}
