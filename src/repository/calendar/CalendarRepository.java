package repository.calendar;

import models.calendar.Calendar;
import repository.generic.GenericRepository;

public interface CalendarRepository extends GenericRepository<Calendar, Integer>{

}
