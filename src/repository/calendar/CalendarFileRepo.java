package repository.calendar;

import models.calendar.Calendar;
import repository.generic.GenericFileRepo;

public class CalendarFileRepo extends GenericFileRepo<Calendar, Integer> implements CalendarRepository{

	public CalendarFileRepo(String filePath) {
		super(filePath);
	}

}
