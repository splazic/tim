package repository.notification;

import models.notification.Notification;
import repository.generic.GenericRepository;

public interface NotificationRepository extends GenericRepository<Notification, Integer>{

}
