package repository.notification;

import models.notification.Notification;
import repository.generic.GenericFileRepo;

public class NotificationFileRepo extends GenericFileRepo<Notification, Integer> implements NotificationRepository{

	public NotificationFileRepo(String filePath) {
		super(filePath);
	}

}
