package repository.appointment.time_slot;

import models.appointment.TimeSlot;
import repository.generic.GenericFileRepo;

public class TimeSlotFileRepo extends GenericFileRepo<TimeSlot, Integer> implements TimeSlotRepository{
	
	public TimeSlotFileRepo(String filePath) {
		super(filePath);
	}
}
