package repository.appointment.time_slot;

import models.appointment.TimeSlot;
import repository.generic.GenericRepository;

public interface TimeSlotRepository extends GenericRepository<TimeSlot, Integer>{

}
