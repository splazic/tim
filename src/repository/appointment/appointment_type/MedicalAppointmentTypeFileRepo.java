package repository.appointment.appointment_type;

import models.appointment.type.MedicalAppointmentType;
import repository.generic.GenericFileRepo;

public class MedicalAppointmentTypeFileRepo extends GenericFileRepo<MedicalAppointmentType, Integer> implements MedicalAppointmentTypeRepository{

	public MedicalAppointmentTypeFileRepo(String filePath) {
		super(filePath);
	}

}
