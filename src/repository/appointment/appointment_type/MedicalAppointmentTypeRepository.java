package repository.appointment.appointment_type;

import models.appointment.type.MedicalAppointmentType;
import repository.generic.GenericRepository;

public interface MedicalAppointmentTypeRepository extends GenericRepository<MedicalAppointmentType, Integer>{

}
