package repository.appointment.appointment_time_slot;

import java.util.ArrayList;
import java.util.List;

import models.appointment.AppointmentTimeSlot;
import repository.generic.GenericFileRepo;

public class AppointmentTimeSlotFileRepo extends GenericFileRepo<AppointmentTimeSlot, Integer> implements AppointmentTimeSlotRepository{

	public AppointmentTimeSlotFileRepo(String filePath) {
		super(filePath);
	}

	@Override
	public List<AppointmentTimeSlot> getAll(int appointmentId) {
		List<AppointmentTimeSlot> list = new ArrayList<AppointmentTimeSlot>();
		for (AppointmentTimeSlot appointmentTimeSlot : list) {
			if(appointmentTimeSlot.getAppointmentId() == appointmentId) {
				list.add(appointmentTimeSlot);
			}
		}
		return list;
	}

}
