package repository.appointment.appointment_time_slot;

import java.util.List;

import models.appointment.AppointmentTimeSlot;
import repository.generic.GenericRepository;

public interface AppointmentTimeSlotRepository extends GenericRepository<AppointmentTimeSlot, Integer>{
	List<AppointmentTimeSlot> getAll(int appointmentId);
}
