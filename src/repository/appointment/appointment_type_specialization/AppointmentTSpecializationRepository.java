package repository.appointment.appointment_type_specialization;

import models.appointment.AppointmentTypeSpecialization;
import repository.generic.GenericRepository;

public interface AppointmentTSpecializationRepository extends GenericRepository<AppointmentTypeSpecialization, Integer>{

}
