package repository.appointment.appointment_type_specialization;

import models.appointment.AppointmentTypeSpecialization;
import repository.generic.GenericFileRepo;
import service.appointment.IAppointmentTSpecializationService;

public class AppointmentTSpecializationFileRepo extends GenericFileRepo<AppointmentTypeSpecialization, Integer> implements IAppointmentTSpecializationService{

	public AppointmentTSpecializationFileRepo(String filePath) {
		super(filePath);
	}

}
