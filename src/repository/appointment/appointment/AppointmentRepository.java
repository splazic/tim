package repository.appointment.appointment;

import java.time.LocalDate;
import java.util.List;

import models.appointment.Appointment;
import repository.generic.GenericRepository;

public interface AppointmentRepository extends GenericRepository<Appointment, Integer>{
	List<Appointment> getScheduledAppointmentsFromDate(LocalDate date);
	public List<Appointment> getScheduledAppointments(String roomId);
}
