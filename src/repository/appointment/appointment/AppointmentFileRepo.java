package repository.appointment.appointment;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import models.appointment.Appointment;
import models.appointment.AppointmentStatus;
import repository.generic.GenericFileRepo;

public class AppointmentFileRepo extends GenericFileRepo<Appointment, Integer> implements AppointmentRepository {

	public AppointmentFileRepo(String filePath) {
		super(filePath);
	}

	@Override
	public List<Appointment> getScheduledAppointmentsFromDate(LocalDate date) {
		List<Appointment> list = new ArrayList<Appointment>();
		for (Appointment appointment : getAll()) {
			if (appointment.getDate().isEqual(date) || appointment.getDate().isAfter(date)) {
				if (appointment.getAppointmentStatus() == AppointmentStatus.SCHEDULED)
					list.add(appointment);
			}
		}
		return list;
	}

	@Override
	public List<Appointment> getScheduledAppointments(String roomId) {
		List<Appointment> list = new ArrayList<Appointment>();
		LocalDate date = LocalDate.now();
		for (Appointment appointment : getAll()) {
			if (appointment.getRoomId().equals(roomId)) {
				if (appointment.getDate().isEqual(date) || appointment.getDate().isAfter(date)) {
					if (appointment.getAppointmentStatus() == AppointmentStatus.SCHEDULED)
						list.add(appointment);
				}
			}
		}
		return list;
	}

}
