package repository.generic;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import exceptions.ResourceNotFound;
import models.Entity;
import utility.FileManager;

public abstract class GenericFileRepo<T extends Entity<ID>, ID> implements GenericRepository<T, ID> {

	private String filePath;
	private FileManager fileManager;

	@SuppressWarnings("unchecked")
	Class<T> type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	public GenericFileRepo(String filePath) {
		this.filePath = filePath;
		fileManager = FileManager.getInstance();
	}

	public T update(T object) throws ResourceNotFound {
		List<T> list = fileManager.readObjectsFromFile(filePath);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getId().equals(object.getId())) {
				list.set(i, object);
				fileManager.saveObjectsToFile(filePath, list);
				return object;
			}
		}
		throw new ResourceNotFound("Resurs sa id: [" + object.getId() + "] nije pronadjen u fajlu '" + filePath + "'");
	}

	public T delete(T object) throws ResourceNotFound {
		List<T> list = fileManager.readObjectsFromFile(filePath);
		for (T obj : list) {
			if (obj.getId().equals(object.getId())) {
				obj.setDeleted(true);
				object.setDeleted(true);
			}
		}
		if (!object.isDeleted()) {
			throw new ResourceNotFound(
					"Resurs sa id: [" + object.getId() + "] nije pronadjen u fajlu '" + filePath + "'");
		}
		fileManager.saveObjectsToFile(filePath, list);
		return object;
	}

	@SuppressWarnings("unchecked")
	private void genID(T object, List<T> list) {
		if ((object.getId() instanceof Integer)) {
			Integer size = list.size();
			object.setId((ID) size);
		}
	}

	public T save(T object) {
		List<T> list = fileManager.readObjectsFromFile(filePath);
		genID(object, list);
		list.add(object);
		fileManager.saveObjectsToFile(filePath, list);
		return object;
	}

	public List<T> getAll() {
		List<T> list = fileManager.readObjectsFromFile(filePath);
		List<T> list2 = new ArrayList<T>();
		for (T obj : list) {
			if (type.isInstance(obj) && !obj.isDeleted())
				list2.add((T) obj);
		}
		return list2;
	}

	public T getById(ID id) throws ResourceNotFound {
		List<T> list = fileManager.readObjectsFromFile(filePath);
		for (T obj : list) {
			if (obj.getId().equals(id) && type.isInstance(obj))
				return obj;
		}
		throw new ResourceNotFound("Resurs sa id: [" + id + "] nije pronadjen u fajlu '" + filePath + "'");
	}
}
