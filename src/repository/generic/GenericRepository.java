package repository.generic;

import java.util.List;

import javax.management.InvalidAttributeValueException;

import exceptions.ResourceNotFound;
import models.Entity;

public interface GenericRepository<T extends Entity<ID>, ID> {
	T update(T object) throws ResourceNotFound, InvalidAttributeValueException;

	T delete(T object) throws ResourceNotFound;

	T save(T object) throws InvalidAttributeValueException;

	List<T> getAll();

	T getById(ID id) throws ResourceNotFound;
}
