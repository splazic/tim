/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.user.doctor;

import models.user.doctor.Specialisation;
import repository.generic.GenericRepository;

/**
 *
 * @author stefan
 */
public interface SpecialisationRepository extends GenericRepository<Specialisation, Integer> {

}
