/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.user.doctor;

import models.user.doctor.DoctorSpecialisation;
import repository.generic.GenericRepository;

/**
 *
 * @author stefan
 */
public interface DoctorSpecialisationRepository extends GenericRepository<DoctorSpecialisation, Integer> {

}
