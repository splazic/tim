package repository.user.doctor;

import models.user.doctor.Doctor;
import repository.generic.GenericRepository;

public interface DoctorRepository extends GenericRepository<Doctor, Integer>{

}
