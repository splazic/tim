package repository.user.doctor;

import models.user.doctor.Doctor;
import repository.generic.GenericFileRepo;

public class DoctorFileRepo extends GenericFileRepo<Doctor, Integer> implements DoctorRepository{

	public DoctorFileRepo(String filePath) {
		super(filePath);
	}

}
