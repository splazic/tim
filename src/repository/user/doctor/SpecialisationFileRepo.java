/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository.user.doctor;

import models.user.doctor.Specialisation;
import repository.generic.GenericFileRepo;

/**
 *
 * @author stefan
 */
public class SpecialisationFileRepo extends GenericFileRepo<Specialisation, Integer> implements SpecialisationRepository {

    public SpecialisationFileRepo(String filePath) {
        super(filePath);
    }

}
