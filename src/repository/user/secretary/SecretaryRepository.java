package repository.user.secretary;

import models.user.Secretary;
import repository.generic.GenericRepository;

public interface SecretaryRepository extends GenericRepository<Secretary, Integer>{

}
