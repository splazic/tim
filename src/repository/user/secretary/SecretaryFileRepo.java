package repository.user.secretary;

import models.user.Secretary;
import service.crud.CRUDService;

public class SecretaryFileRepo extends CRUDService<Secretary, Integer, SecretaryRepository>{

	public SecretaryFileRepo(SecretaryRepository repo) {
		super(repo);
	}

}
