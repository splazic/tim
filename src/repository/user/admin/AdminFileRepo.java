package repository.user.admin;

import models.user.Administrator;
import repository.generic.GenericFileRepo;

public class AdminFileRepo extends GenericFileRepo<Administrator, Integer> implements AdministratorRepository{

	public AdminFileRepo(String filePath) {
		super(filePath);
	}

}
