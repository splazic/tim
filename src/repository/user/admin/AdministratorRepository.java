package repository.user.admin;

import models.user.Administrator;
import repository.generic.GenericRepository;

public interface AdministratorRepository extends GenericRepository<Administrator, Integer>{

}
