package repository.user.user;

import java.util.List;
import exceptions.ResourceNotFound;
import models.user.User;
import repository.generic.GenericFileRepo;
import utility.FileManager;

public class UserFileRepo extends GenericFileRepo<User, Integer> implements UserRepository {

	private String filePath;
	FileManager fileManager;

	public UserFileRepo(String filePath) {
		super(filePath);
		this.filePath = filePath;
		fileManager = FileManager.getInstance();
	}

	@Override
	public User getByUsername(String username) throws ResourceNotFound {
		List<User> list = fileManager.readObjectsFromFile(filePath);
		for (User user : list) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		throw new ResourceNotFound(
				"Korisnik sa  korisnickim imenom: [" + username + "] nije pronadjen u fajlu '" + filePath + "'");
	}
}
