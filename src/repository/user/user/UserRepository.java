package repository.user.user;

import exceptions.ResourceNotFound;
import models.user.User;
import repository.generic.GenericRepository;

public interface UserRepository extends GenericRepository<User, Integer>{
	User getByUsername(String username) throws ResourceNotFound;
}
