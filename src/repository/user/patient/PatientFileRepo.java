package repository.user.patient;

import models.user.patient.Patient;
import repository.generic.GenericFileRepo;

public class PatientFileRepo extends GenericFileRepo<Patient, Integer> implements PatientRepository{

	
	public PatientFileRepo(String filePath) {
		super(filePath);
	}

}
