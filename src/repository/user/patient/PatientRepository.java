package repository.user.patient;

import models.user.patient.Patient;
import repository.generic.GenericRepository;

public interface PatientRepository extends GenericRepository<Patient, Integer>{

}
