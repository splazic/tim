package repository.user.manager;

import models.user.Manager;
import repository.generic.GenericRepository;

public interface ManagerRepository extends GenericRepository<Manager, Integer>{
	
}
