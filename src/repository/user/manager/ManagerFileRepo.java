package repository.user.manager;

import models.user.Manager;
import repository.generic.GenericFileRepo;

public class ManagerFileRepo extends GenericFileRepo<Manager, Integer> implements ManagerRepository{

	public ManagerFileRepo(String filePath) {
		super(filePath);
	}


}
