package repository.hospital_equipment.required_equipment;

import java.util.List;

import models.equipment.RequiredEquipment;
import repository.generic.GenericRepository;

public interface RequiredEquipmentRepository extends GenericRepository<RequiredEquipment, Integer>{
	public List<RequiredEquipment> getAll(int equipmentId);
}
