package repository.hospital_equipment.required_equipment;

import java.util.ArrayList;
import java.util.List;

import models.equipment.RequiredEquipment;
import repository.generic.GenericFileRepo;

public class RequiredEquFileRepo extends GenericFileRepo<RequiredEquipment, Integer> implements RequiredEquipmentRepository{

	public RequiredEquFileRepo(String filePath) {
		super(filePath);
	}
	
	@Override
	public List<RequiredEquipment> getAll(int equipmentId){
		List<RequiredEquipment> list = new ArrayList<RequiredEquipment>();
		for (RequiredEquipment requiredEquipment : getAll()) {
			if(requiredEquipment.getEquipmentId() == equipmentId) {
				list.add(requiredEquipment);
			}
		}
		return list;
	}
}
