package repository.hospital_equipment.equipment;

import exceptions.ResourceNotFound;
import models.equipment.HospitalEquipment;
import repository.generic.GenericFileRepo;

public class HospitalEquipmentFileRepo extends GenericFileRepo<HospitalEquipment, Integer> implements HospitalEquipmentRepository {

	public HospitalEquipmentFileRepo(String filePath) {
		super(filePath);
	}

	@Override
	public HospitalEquipment getById(Integer id) throws ResourceNotFound {
		HospitalEquipment equipment = super.getById(id);
		if(equipment.isDeleted())
			throw new ResourceNotFound("Oprema sa id - "+id+" nije registrovana u sistemu.");
		return equipment;
	}
	
	@Override
	public HospitalEquipment getByEquipmentName(String name) throws ResourceNotFound {
		for (HospitalEquipment hEquipment : getAll()) {
			if (hEquipment.getName().equals(name)) {
				return hEquipment;
			}
		}
		throw new ResourceNotFound("Oprema sa nazivom '" + name + "' nije upisana u sistem.");
	}
}
