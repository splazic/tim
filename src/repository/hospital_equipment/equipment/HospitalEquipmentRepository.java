package repository.hospital_equipment.equipment;

import exceptions.ResourceNotFound;
import models.equipment.HospitalEquipment;
import repository.generic.GenericRepository;

public interface HospitalEquipmentRepository  extends GenericRepository<HospitalEquipment, Integer>{
	public HospitalEquipment getByEquipmentName(String name) throws ResourceNotFound;
}
