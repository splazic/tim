package repository.room.room;

import java.util.ArrayList;
import java.util.List;

import exceptions.ResourceNotFound;
import models.room.Room;
import repository.generic.GenericFileRepo;
import utility.Validation;

public class RoomFileRepo extends GenericFileRepo<Room, String> implements RoomRepository {

	private String roomFilePath;
	private Validation validation = Validation.getInstance();

	public RoomFileRepo(String filePath) {
		super(filePath);
		this.roomFilePath = filePath;
	}

	@Override
	public Room getById(String id) throws ResourceNotFound {
		List<Room> list = super.getAll();
		for (Room room : list) {
			if (room.getId().equals(id)) {
				if (validation.isDateValid(room.getCreatedDate(), room.getLastModifiedDate())) {
					return room;
				}
			}
		}
		throw new ResourceNotFound("Resurs sa id: [" + id + "] nije pronadjen u fajlu '" + roomFilePath + "'");
	}
	
	@Override
	public List<Room> getAll() {
		List<Room> roomList = super.getAll();
		List<Room> allRooms = new ArrayList<Room>();
		for (Room room : roomList) {
			if (validation.isDateValid(room.getCreatedDate(), room.getLastModifiedDate())) {
				allRooms.add(room);
			}
		}
		return allRooms;
	}
}
