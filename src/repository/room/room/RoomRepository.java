package repository.room.room;

import models.room.Room;
import repository.generic.GenericRepository;

public interface RoomRepository extends GenericRepository<Room, String> {
}
