package repository.room.room_equipment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dto.date.DateDTO;
import dto.room.RoomDTO;
import dto.room.RoomEquDTO;
import exceptions.ResourceNotFound;
import models.room.RoomHospitalEquipment;
import repository.generic.GenericFileRepo;
import utility.Validation;

public class RoomEquipmentFileRepo extends GenericFileRepo<RoomHospitalEquipment, Integer>
		implements RoomEquipmentRepository {

	private Validation validation;

	public RoomEquipmentFileRepo(String filePath) {
		super(filePath);
		this.validation = Validation.getInstance();
	}

	public void deleteAll(int equipmentId) throws ResourceNotFound {
		for (RoomHospitalEquipment roomEquipment : getAll()) {
			if (roomEquipment.getHospitalEquipmentId() == equipmentId)
				super.delete(roomEquipment);
		}
	}

	@Override
	public List<RoomHospitalEquipment> getReservedEquipment(int equipmentId) {
		List<RoomHospitalEquipment> list = new ArrayList<RoomHospitalEquipment>();
		Date currentDate = new Date();
		for (RoomHospitalEquipment roomEquipment : getAll()) {
			if (roomEquipment.getHospitalEquipmentId() == equipmentId) {
				Date startD = roomEquipment.getCreatedDate();
				Date endD = roomEquipment.getLastModifiedDate();
				boolean isEqual = false;
				if (validation.isDateValid(startD, endD) || startD.after(currentDate)) {
					for (int i = 0; i < list.size(); ++i) {
						if (roomEquipment.getRoomId().equals(list.get(i).getRoomId())) {
							if (roomEquipment.getQuantity() > list.get(i).getQuantity())
								list.set(i, roomEquipment);
							isEqual = true;
						}
					}
					if (!isEqual)
						list.add(roomEquipment);
				}
			}
		}
		return list;
	}
	
	private void deleteAll(String roomId, Date date) throws ResourceNotFound {
		for (RoomHospitalEquipment hEqu : getAll()) {
			if(hEqu.getRoomId().equals(roomId) && hEqu.getLastModifiedDate() == null) {
				hEqu.setLastModifiedDate(date);
				super.update(hEqu);
			}
		}
	};
	
	public void updateRoomEquipment(DateDTO date, RoomDTO roomEqu) throws ResourceNotFound {
		String roomId = roomEqu.getRoom().getId(); 
		deleteAll(roomId , date.getStartDate());
		for (RoomEquDTO equ : roomEqu.getEquipmentList()) {
			int hospitalEquipmentId = equ.getEquipment().getId();
			Date createdDate = date.getStartDate();
			Date lastModifiedDate = null;
			int quantity = equ.getQuantity();
			RoomHospitalEquipment hEqu = new RoomHospitalEquipment(roomId, hospitalEquipmentId, createdDate, lastModifiedDate, quantity);
			super.save(hEqu);
		}
	}

}
