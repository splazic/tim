package repository.room.room_equipment;

import java.util.List;

import dto.date.DateDTO;
import dto.room.RoomDTO;
import exceptions.ResourceNotFound;
import models.room.RoomHospitalEquipment;
import repository.generic.GenericRepository;

public interface RoomEquipmentRepository extends GenericRepository<RoomHospitalEquipment, Integer> {
	public List<RoomHospitalEquipment> getReservedEquipment(int equipmentId);
	public void deleteAll(int equipmentId) throws ResourceNotFound;
	public void updateRoomEquipment(DateDTO date, RoomDTO roomEqu) throws ResourceNotFound;
}
