/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.notification;

import java.time.LocalDate;

/**
 *
 * @author stefan
 */
public class ScheduleNotification extends Notification {

    private int doctorId;
    private int patientId;
    private LocalDate startDate;
    private LocalDate endDate;

    public ScheduleNotification(int doctorId, int patientId, LocalDate startDate, LocalDate endDate) {
        this.doctorId = doctorId;
        this.patientId = patientId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

}
