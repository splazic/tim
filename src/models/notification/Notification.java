package models.notification;

import models.Entity;

public abstract class Notification extends Entity<Integer> {

    /**
     *
     */
    private static final long serialVersionUID = -3567801415732238629L;
    private boolean isChecked;

    public Notification() {
        super(0);
        this.isChecked = false;
    }

    public boolean isIsChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

}
