/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.notification;

/**
 *
 * @author stefan
 */
public class MessageNotification extends Notification {

    private int appointmentId;
    private String message;

    public MessageNotification(int appointmentId, String message) {
        this.appointmentId = appointmentId;
        this.message = message;
    }

    public int getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
