/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.room;

import java.io.Serializable;
import java.util.Date;

import models.Entity;

/**
 *
 * @author stefan
 */
public class RoomHospitalEquipment extends Entity<Integer> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4875262533134190839L;
	private String roomId;
	private Integer hospitalEquipmentId;
	private Date createdDate;
	private Date lastModifiedDate;
	private int quantity;

	public RoomHospitalEquipment() {
		super(null, false);
	}

	public RoomHospitalEquipment(String roomId, Integer hospitalEquipmentId, Date createdDate,
			Date lastModifiedDate, int quantity) {
		super(0);
		this.roomId = roomId;
		this.hospitalEquipmentId = hospitalEquipmentId;
		this.createdDate = createdDate;
		this.lastModifiedDate = lastModifiedDate;
		this.quantity = quantity;
	}
	
	public RoomHospitalEquipment(Integer id, String roomId, Integer hospitalEquipmentId, Date createdDate,
			Date lastModifiedDate, int quantity, boolean deleted) {
		super(id, deleted);
		this.roomId = roomId;
		this.hospitalEquipmentId = hospitalEquipmentId;
		this.createdDate = createdDate;
		this.lastModifiedDate = lastModifiedDate;
		this.quantity = quantity;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public Integer getHospitalEquipmentId() {
		return hospitalEquipmentId;
	}

	public void setHospitalEquipmentId(Integer hospitalEquipmentId) {
		this.hospitalEquipmentId = hospitalEquipmentId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
