/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.room;

import java.io.Serializable;
import java.util.Date;

import models.Entity;

/**
 *
 * @author stefan
 */
public class Room extends Entity<String> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7787373978686618037L;
	private RoomType roomType;
	private Date createdDate;
	private Date lastModifiedDate;

	public Room(String id, boolean deleted, RoomType roomType, Date createdDate, Date lastModifiedDate) {
		super(id, deleted);
		this.roomType = roomType;
		this.createdDate = createdDate;
		this.lastModifiedDate = lastModifiedDate;
	}

	public Room(Room room) {
		super(room.getId(), room.isDeleted());
		this.roomType = room.getRoomType();
		this.createdDate = room.getCreatedDate();
		this.lastModifiedDate = room.getLastModifiedDate();
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String RoomTypeToString() {
		if (this.getRoomType() == RoomType.EXAMINATION)
			return "SALA ZA PREGLEDE";
		else if (this.getRoomType() == RoomType.OPERATION)
			return "OPERACIONA SALA";
		else
			return "SOBA ZA LEZANJE";
	}

	@Override
	public String toString() {
		return "Room [roomType=" + roomType + ", createdDate=" + createdDate + ", lastModifiedDate=" + lastModifiedDate
				+ ", getId()=" + getId() + ", isDeleted()=" + isDeleted() + "]";
	}

}
