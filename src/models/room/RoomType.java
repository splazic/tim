/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.room;

import java.io.Serializable;

/**
 *
 * @author stefan
 */
public enum RoomType implements Serializable {
    OPERATION,
    EXAMINATION,
    REST
}
