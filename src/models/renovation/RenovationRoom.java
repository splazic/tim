package models.renovation;

import models.Entity;

public class RenovationRoom extends Entity<Integer>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8635601062478289598L;
	private int renovationId;
	private String roomId;

	public RenovationRoom(int id, boolean deleted, int renovationId, String roomId) {
		super(id, deleted);
		this.renovationId = renovationId;
		this.roomId = roomId;
	}
	
	public RenovationRoom(int renovationId, String roomId) {
		super(0);
		this.renovationId = renovationId;
		this.roomId = roomId;
	}

	public int getRenovationId() {
		return renovationId;
	}

	public void setRenovationId(int renovationId) {
		this.renovationId = renovationId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
