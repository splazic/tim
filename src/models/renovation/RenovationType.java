package models.renovation;

public enum RenovationType {
	UPDATE_ROOM_TYPE,
	UPDATE_ROOM_EQU,
	SPLIT_ROOM,
	MERGE_ROOM,
	NONE
}
