package models.renovation;

import java.util.Date;

import models.Entity;

public class Renovation extends Entity<Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1760287568455462L;
	private int mangerId;
	private Date renovationStartDate;
	private Date renovatinoEndDate;
	private RenovationType renovationType;
	
	public Renovation() {
		super(0);
		// TODO Auto-generated constructor stub
	}

	public Renovation(int id, int mangerId, Date renovationStartDate, Date renovatinoEndDate,
			RenovationType renovationType, boolean deleted) {
		super(id, deleted);
		this.mangerId = mangerId;
		this.renovationStartDate = renovationStartDate;
		this.renovatinoEndDate = renovatinoEndDate;
		this.renovationType = renovationType;
	}
	
	public Renovation(int mangerId, Date renovationStartDate, Date renovatinoEndDate,
			RenovationType renovationType) {
		super(0);
		this.mangerId = mangerId;
		this.renovationStartDate = renovationStartDate;
		this.renovatinoEndDate = renovatinoEndDate;
		this.renovationType = renovationType;
	}

	public RenovationType getRenovationType() {
		return renovationType;
	}

	public void setRenovationType(RenovationType renovationType) {
		this.renovationType = renovationType;
	}

	public int getMangerId() {
		return mangerId;
	}

	public void setMangerId(int mangerId) {
		this.mangerId = mangerId;
	}

	public Date getRenovationStartDate() {
		return renovationStartDate;
	}

	public void setRenovationStartDate(Date renovationStartDate) {
		this.renovationStartDate = renovationStartDate;
	}

	public Date getRenovatinoEndDate() {
		return renovatinoEndDate;
	}

	public void setRenovatinoEndDate(Date renovatinoEndDate) {
		this.renovatinoEndDate = renovatinoEndDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
