/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.equipment;
import models.Entity;

/**
 *
 * @author stefan
 */
public class HospitalEquipment extends Entity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3271181189143129228L;
	private String name;
	private String description;
	private int totalEquipment;

	public HospitalEquipment() {
		super(0);
		// TODO Auto-generated constructor stub
	}
	
	public HospitalEquipment(Integer id, String name, String description, int totalEquipment, boolean deleted) {
		super(id, deleted);
		this.name = name;
		this.description = description;
		this.totalEquipment = totalEquipment;
	}
	
	public HospitalEquipment(String name, String description, int totalEquipment) {
		super(0);
		this.name = name;
		this.description = description;
		this.totalEquipment = totalEquipment;
	}

	public HospitalEquipment(HospitalEquipment hEquipment) {
		super(hEquipment.getId(), hEquipment.isDeleted());
		this.name = hEquipment.getName();
		this.description = hEquipment.getDescription();
		this.totalEquipment = hEquipment.getTotalEquipment();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalEquipment() {
		return totalEquipment;
	}

	public void setTotalEquipment(int totalEquipment) {
		this.totalEquipment = totalEquipment;
	}

	@Override
	public String toString() {
		return "HospitalEquipment [name=" + name + ", description=" + description + ", totalEquipment=" + totalEquipment
				+ ", getId()=" + getId() + ", isDeleted()=" + isDeleted();
	}

}
