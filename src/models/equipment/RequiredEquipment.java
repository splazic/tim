package models.equipment;

import models.Entity;

public class RequiredEquipment extends Entity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 195192959064185377L;
	private int medAppointmentTypeId;
	private int equipmentId;
	private int quantity;

	public RequiredEquipment(int medAppointmentTypeId, int equipmentId, int quantity) {
		super(0);
		this.medAppointmentTypeId = medAppointmentTypeId;
		this.equipmentId = equipmentId;
		this.quantity = quantity;
	}

	public RequiredEquipment(Integer id, Boolean deleted, int medAppointmentTypeId, int equipmentId, int quantity) {
		super(id, deleted);
		this.medAppointmentTypeId = medAppointmentTypeId;
		this.equipmentId = equipmentId;
		this.quantity = quantity;
	}

	public int getMedAppointmentTypeId() {
		return medAppointmentTypeId;
	}

	public void setMedAppointmentTypeId(int medAppointmentTypeId) {
		this.medAppointmentTypeId = medAppointmentTypeId;
	}

	public int getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(int equipmentId) {
		this.equipmentId = equipmentId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
