package models.appointment.type;

import models.Entity;

public class MedicalAppointmentType extends Entity<Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7262289478943242322L;
	private String name;
	private String description;
	private AppointmentType appointmentType;

	public MedicalAppointmentType() {
		super(0);
	}

	public MedicalAppointmentType(int id, boolean deleted, String name, String description,
			AppointmentType appointmentType) {
		super(id, deleted);
		this.name = name;
		this.description = description;
		this.appointmentType = appointmentType;
	}

	public MedicalAppointmentType(String name, String description, AppointmentType appointmentType) {
		super(0);
		this.name = name;
		this.description = description;
		this.appointmentType = appointmentType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AppointmentType getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(AppointmentType appointmentType) {
		this.appointmentType = appointmentType;
	}

}
