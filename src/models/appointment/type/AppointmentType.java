package models.appointment.type;

public enum AppointmentType {
	EXAMINATION,
	OPERATION
}
