package models.appointment;

import models.Entity;

public class AppointmentTimeSlot extends Entity<Integer>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3117913871545317765L;
	private int appointmentId;
	private int timeSlotId;

	public AppointmentTimeSlot(int id, boolean deleted, int appointmentId, int timeSlotId) {
		super(id, deleted);
		this.appointmentId = appointmentId;
		this.timeSlotId = timeSlotId;
	}
	
	public AppointmentTimeSlot(int appointmentId, int timeSlotId) {
		super(0);
		this.appointmentId = appointmentId;
		this.timeSlotId = timeSlotId;
	}

	public int getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(int appointmentId) {
		this.appointmentId = appointmentId;
	}

	public int getTimeSlotId() {
		return timeSlotId;
	}

	public void setTimeSlotId(int timeSlotId) {
		this.timeSlotId = timeSlotId;
	}

}
