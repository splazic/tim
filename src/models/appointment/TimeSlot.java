package models.appointment;

import java.time.LocalTime;

import models.Entity;

public class TimeSlot extends Entity<Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7979506560430691603L;
	private LocalTime startTime;
	private LocalTime endTime;

	public TimeSlot(LocalTime startTime, LocalTime endTime) {
		super(0);
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public TimeSlot(int id, boolean deleted, LocalTime startTime, LocalTime endTime) {
		super(id, deleted);
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

}
