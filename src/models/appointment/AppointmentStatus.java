package models.appointment;

public enum AppointmentStatus {
	SCHEDULED,
	CANCELED,
	FINISHED
}
