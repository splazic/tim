package models.appointment;

import java.time.LocalDate;

import models.Entity;

public class Appointment extends Entity<Integer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8803892413815577303L;
	private int userId;
	private int doctorId;
	private String roomId;
	private LocalDate date;
	private AppointmentStatus appointmentStatus;
	private int medAppointmentTypeId;

	public Appointment(Integer id, Boolean deleted, int userId, int doctorId, String roomId, LocalDate date,
			AppointmentStatus appointmentStatus, int medAppointmentTypeId) {
		super(id, deleted);
		this.userId = userId;
		this.doctorId = doctorId;
		this.roomId = roomId;
		this.date = date;
		this.appointmentStatus = appointmentStatus;
		this.medAppointmentTypeId = medAppointmentTypeId;
	}

	public Appointment(int userId, int doctorId, String roomId, LocalDate date, AppointmentStatus appointmentStatus,
			int medAppointmentTypeId) {
		super(0);
		this.userId = userId;
		this.doctorId = doctorId;
		this.roomId = roomId;
		this.date = date;
		this.appointmentStatus = appointmentStatus;
		this.medAppointmentTypeId = medAppointmentTypeId;
	}

	public AppointmentStatus getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public int getMedAppointmentTypeId() {
		return medAppointmentTypeId;
	}

	public void setMedAppointmentTypeId(int medAppointmentTypeId) {
		this.medAppointmentTypeId = medAppointmentTypeId;
	}

}
