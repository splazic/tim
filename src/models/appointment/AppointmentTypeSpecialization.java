package models.appointment;

import models.Entity;

public class AppointmentTypeSpecialization extends Entity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5166874544845417367L;
	private int appointmentTypeId;
	private int specializationId;

	public AppointmentTypeSpecialization(int appointmentTypeId, int specializationId) {
		super(0);
		this.appointmentTypeId = appointmentTypeId;
		this.specializationId = specializationId;
	}

	public AppointmentTypeSpecialization(Integer id, Boolean deleted, int appointmentTypeId, int specializationId) {
		super(id, deleted);
		this.appointmentTypeId = appointmentTypeId;
		this.specializationId = specializationId;
	}

	public int getAppointmentTypeId() {
		return appointmentTypeId;
	}

	public void setAppointmentTypeId(int appointmentTypeId) {
		this.appointmentTypeId = appointmentTypeId;
	}

	public int getSpecializationId() {
		return specializationId;
	}

	public void setSpecializationId(int specializationId) {
		this.specializationId = specializationId;
	}

}
