package models.calendar;

import java.time.LocalDate;
import java.time.LocalTime;

import models.Entity;

public class Calendar extends Entity<Integer>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -198492616084471237L;
	private int userId;
	private LocalDate date;
	private LocalTime startTime;
	private LocalTime endTime;

	public Calendar(int id, boolean deleted, int userId, LocalDate date, LocalTime startTime, LocalTime endTime) {
		super(id, deleted);
		this.userId = userId;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public Calendar(int userId, LocalDate date, LocalTime startTime, LocalTime endTime) {
		super(0);
		this.userId = userId;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
}
