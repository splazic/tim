/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user.patient;

import java.util.Date;

import models.user.User;

/**
 *
 * @author stefan
 */
public class Patient extends User {

	private static final long serialVersionUID = -6729020204895920087L;
	private String medicalNumber;
	private boolean isMale;
	private int medicalRecordId;
	private Date dateOfBirth;

	public Patient(String medicalNumber, boolean isMale, int medicalRecordId, Date dateOfBirth, int id, String username,
			String password, String firstName, String lastName, boolean deleted) {
		super(id, deleted, username, password, firstName, lastName);
		this.medicalNumber = medicalNumber;
		this.isMale = isMale;
		this.medicalRecordId = medicalRecordId;
		this.dateOfBirth = dateOfBirth;
	}
	
	public Patient(String medicalNumber, boolean isMale, int medicalRecordId, Date dateOfBirth, String username,
			String password, String firstName, String lastName) {
		super(username, password, firstName, lastName);
		this.medicalNumber = medicalNumber;
		this.isMale = isMale;
		this.medicalRecordId = medicalRecordId;
		this.dateOfBirth = dateOfBirth;
	}

	public Patient(Patient patient) {
		super(patient.getId(), patient.isDeleted(), patient.getUsername(), patient.getPassword(),
				patient.getFirstName(), patient.getLastName());
		this.medicalNumber = patient.getMedicalNumber();
		this.isMale = patient.isIsMale();
		this.medicalRecordId = patient.getMedicalRecordId();
		this.dateOfBirth = patient.getDateOfBirth();
	}

	@Override
	public User copy() {
		// TODO Auto-generated method stub
		return new Patient(this);
	}

	@Override
	public String toString() {
		return "[Patient] " + super.toString() + " [medicalNumber=" + medicalNumber + ", isMale=" + isMale
				+ ", medicalRecordId=" + medicalRecordId + ", dateOfBirth=" + dateOfBirth + "]";
	}

	public String getMedicalNumber() {
		return medicalNumber;
	}

	public void setMedicalNumber(String medicalNumber) {
		this.medicalNumber = medicalNumber;
	}

	public boolean isIsMale() {
		return isMale;
	}

	public void setIsMale(boolean isMale) {
		this.isMale = isMale;
	}

	public boolean isMale() {
		return isMale;
	}

	public void setMale(boolean isMale) {
		this.isMale = isMale;
	}

	public int getMedicalRecordId() {
		return medicalRecordId;
	}

	public void setMedicalRecordID(int medicalRecordId) {
		this.medicalRecordId = medicalRecordId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
