/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

/**
 *
 * @author stefan
 */
public class Administrator extends User {

	private static final long serialVersionUID = 7201344037531044060L;

	public Administrator(int id, boolean deleted, String username, String password, String firstName, String lastName) {
		super(id, deleted, username, password, firstName, lastName);
	}
	
	public Administrator(String username, String password, String firstName, String lastName) {
		super(username, password, firstName, lastName);
	}

	public Administrator(Administrator administrator) {
		super(administrator.getId(), administrator.isDeleted(), administrator.getUsername(),
				administrator.getPassword(), administrator.getFirstName(), administrator.getLastName());
	}

	@Override
	public User copy() {
		// TODO Auto-generated method stub
		return new Administrator(this);
	}

	@Override
	public String toString() {
		return "[Administrator] "+super.toString();
	}

}
