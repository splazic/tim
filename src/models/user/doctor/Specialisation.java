/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user.doctor;
import models.Entity;

/**
 *
 * @author stefan
 */
public class Specialisation extends Entity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;

	public Specialisation(int id, boolean deleted, String name, String description) {
		super(id, deleted);
		this.name = name;
		this.description = description;
	}

	public Specialisation(String name, String description) {
		super(0);
		this.name = name;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
