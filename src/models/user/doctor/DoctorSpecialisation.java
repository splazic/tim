/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user.doctor;

import models.Entity;

/**
 *
 * @author stefan
 */
public class DoctorSpecialisation extends Entity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3656177524867462420L;
	private int doctorId;
	private int specialisationId;

	public DoctorSpecialisation(int id, boolean deleted, int doctorId, int specialisationId) {
		super(id, deleted);
		this.doctorId = doctorId;
		this.specialisationId = specialisationId;
	}

	public DoctorSpecialisation(int doctorId, int specialisationId) {
		super(0);
		this.doctorId = doctorId;
		this.specialisationId = specialisationId;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public int getSpecialisationId() {
		return specialisationId;
	}

	public void setSpecialisationId(int specialisationId) {
		this.specialisationId = specialisationId;
	}

}
