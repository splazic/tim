/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user.doctor;

import models.user.User;

/**
 *
 * @author stefan
 */
public class Doctor extends User {

	private static final long serialVersionUID = -8369717779307863038L;

	public Doctor(int id, String username, String password, String firstName, String lastName, boolean deleted) {
		super(id, deleted, username, password, firstName, lastName);
	}
	
	public Doctor(String username, String password, String firstName, String lastName) {
		super(username, password, firstName, lastName);
	}

	public Doctor(Doctor doctor) {
		super(doctor.getId(), doctor.isDeleted(), doctor.getUsername(), doctor.getPassword(), doctor.getFirstName(),
				doctor.getLastName());
	}

	@Override
	public User copy() {
		// TODO Auto-generated method stub
		return new Doctor(this);
	}

	@Override
	public String toString() {
		return "[Doctor] "+super.toString();
	}

}
