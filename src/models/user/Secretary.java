/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

/**
 *
 * @author stefan
 */
public class Secretary extends User {

	private static final long serialVersionUID = -5976387778701219199L;

	public Secretary(int id, boolean deleted, String username, String password, String firstName, String lastName) {
		super(id, deleted, username, password, firstName, lastName);
	}

	public Secretary(String username, String password, String firstName, String lastName) {
		super(username, password, firstName, lastName);
	}
	
	public Secretary(Secretary secretary) {
		super(secretary.getId(), secretary.isDeleted(), secretary.getUsername(), secretary.getPassword(),
				secretary.getFirstName(), secretary.getLastName());
	}

	@Override
	public User copy() {
		// TODO Auto-generated method stub
		return new Secretary(this);
	}

	@Override
	public String toString() {
		return "[Secretary] "+super.toString();
	}

}
