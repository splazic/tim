/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

/**
 *
 * @author stefan
 */
public class Manager extends User {

	private static final long serialVersionUID = 5372514177332091153L;

	public Manager(int id, boolean deleted, String username, String password, String firstName, String lastName) {
		super(id, deleted, username, password, firstName, lastName);
	}
	
	public Manager(String username, String password, String firstName, String lastName) {
		super(username, password, firstName, lastName);
	}

	public Manager(Manager manager) {
		super(manager.getId(), manager.isDeleted(), manager.getUsername(), manager.getPassword(),
				manager.getFirstName(), manager.getLastName());
	}

	@Override
	public User copy() {
		// TODO Auto-generated method stub
		return Manager.this;
	}

	@Override
	public String toString() {
		return "[Manager] "+super.toString();
	}

}
