/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.medical_record;

import java.util.Date;

import models.Entity;

/**
 *
 * @author stefan
 */
public class MedicalRecordItem extends Entity<Integer> {

	/**
	 *
	 */
	private static final long serialVersionUID = -8698663336899576532L;
	private int medicalRecordId;
	private Date createdAt;
	private int doctorId;
	private String description;

	public MedicalRecordItem() {
		super(0, false);
	}

	public MedicalRecordItem(int id, boolean deleted, int medicalRecordId, Date createdAt, int doctorId,
			String description) {
		super(id, deleted);
		this.createdAt = createdAt;
		this.doctorId = doctorId;
		this.description = description;
		this.medicalRecordId = medicalRecordId;
	}

	public MedicalRecordItem(int medicalRecordId, Date createdAt, int doctorId, String description) {
		super(0);
		this.createdAt = createdAt;
		this.doctorId = doctorId;
		this.description = description;
		this.medicalRecordId = medicalRecordId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMedicalRecordId() {
		return medicalRecordId;
	}

	public void setMedicalRecordId(int medicalRecordId) {
		this.medicalRecordId = medicalRecordId;
	}

	@Override
	public String toString() {
		return "\nDatum: " + this.createdAt + "\nOpis: " + this.description;
	}

}
