/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.medical_record;

import models.Entity;

/**
 *
 * @author stefan
 */
/**
 * @author acer
 *
 */
public class MedicalRecord extends Entity<Integer> {

	private static final long serialVersionUID = 667873543496575031L;
	private float height;
	private float weight;

	public MedicalRecord() {
		super(0);
	}

	public MedicalRecord(int id, boolean deleted, float height, float weight) {
		super(id, deleted);
		this.height = height;
		this.weight = weight;
	}

	public MedicalRecord(float height, float weight) {
		super(0);
		this.height = height;
		this.weight = weight;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "MedicalRecord [height=" + height + ", weight=" + weight + "]";
	}
}
