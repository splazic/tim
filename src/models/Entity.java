package models;

import java.io.Serializable;

public abstract class Entity<ID> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 605377777772162322L;
	private ID id;
	private Boolean deleted;

	public Entity(ID id, Boolean deleted) {
		super();
		this.id = id;
		this.deleted = deleted;
	}

	public Entity(ID id) {
		this.id = id;
		this.deleted = false;
	}

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	public Boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
