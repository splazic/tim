package exceptions;

public class ResourceNotFound extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7558378188736005558L;

	public ResourceNotFound(String message) {
		super(message);
	}

}
