package exceptions;

public class DataNotValidException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3924291890929839138L;

	public DataNotValidException(String message) {
		super(message);
	}
}
