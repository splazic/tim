package utility;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

	public static Validation validation = null;

	private Validation() {
	};

	public static Validation getInstance() {
		if (validation == null) {
			validation = new Validation();
		}
		return validation;
	}

	public boolean username(final String string) {
		return string.matches("^[a-z.0-9]*$");
	}

	public boolean isNumber(final String string) {
		return string.matches("^[0-9]*$");
	}

	public boolean isStringOnlyAlphabet(final String string) {
		return string.matches("^[a-zA-Z]*$");
	}

	public boolean length(final String string, int minLength, int maxLength) {
		int length = string.length();
		if (length >= minLength && length <= maxLength)
			return true;
		return false;
	}

	public boolean maxLength(final String string, int maxLength) {
		int length = string.length();
		if (length <= maxLength)
			return true;
		return false;
	}

	public boolean medicalNumberValidation(String medicalNumber) {
		String regex = "^(\\d{13})?$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(medicalNumber);
		return matcher.matches();
	}

	public boolean isDateValid(Date start, Date end) {
		Date currentDate = new Date();
		if (start.before(currentDate) || start.equals(currentDate)) {
			if (end == null || end.after(currentDate)) {
				return true;
			}
		}
		return false;
	}

}
