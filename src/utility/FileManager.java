package utility;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import models.user.User;

public class FileManager {
	private static FileInputStream fileInputStream = null;

	public static final String MEDICALRECORD_URI = "medicalRecord.txt";
	public static final String USER_URI = "user.txt";
	public static final String ROOM_URI = "room.txt";
	public static final String HOSPITAL_EQUIPMENT_URI = "hospitalEquipment.txt";
	public static final String NOTIFICATION_URI = "notification.txt";
	public static final String MEDICAL_APPOINTMENT_URI = "medicalAppointment.txt";
	public static final String WORKING_TIME_URI = "workingTime.txt";
	public static final String TIME_SLOT_URI = "timeSlot.txt";
	public static final String RENOVATION_URI = "renovation.txt";
	public static final String APPOINTMENT_URI = "appointment.txt";
	public static final String APPOINTMENT_TIMESLOT_URI = "appointmentTimeSlot.txt";
	public static final String ROOM_EQUIPMENT_URI = "roomEquipment.txt";
	public static final String ROOM_RENOVATION_URI = "roomRenovation.txt";
	public static final String MEDICAL_RECORD_ITEM_URI = "medicalRecordItem.txt";
	public static final String REQUIRED_EQUIPMENT_URI = "requiredEquipment.txt";
        public static final String SPECIALISATION_URI = "specialisation.txt";
        public static final String DOCTOR_SPECIALISATION_URI = "doctorSpecialisation.txt";
        public static final String MEDICAL_APPPOINTMENT_TYPE_URI = "medicalAppointmentType.txt";

	private static FileManager fileManager;
	public static User loggedUser;

	private FileManager() {
	};

	public static FileManager getInstance() {
		if (fileManager == null) {
			fileManager = new FileManager();
		}
		return fileManager;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> readObjectsFromFile(final String fileUri) {
		List<T> list = new ArrayList<T>();
		ObjectInputStream inputStream = null;
		try {
			inputStream = getObjectInputStream(fileUri);
			while (fileInputStream.available() > 0) {
				Object obj = inputStream.readObject();
				list.add((T) obj);
			}
			inputStream.close();
		} catch (EOFException e) {
			// System.out.println("!!!\nFajl '" + fileUri+"' je prazan.");
		} catch (Exception e) {
			System.out.println("!!!\nGreska prilokom citanja fajla - " + fileUri);
			e.printStackTrace();
		} finally {
			closeObjectInputStream(inputStream);
		}
		return list;
	}

	public <T> void saveObjectsToFile(final String fileUri, final List<T> list) {
		ObjectOutputStream outputStream = null;
		try {
			outputStream = getObjectOutputStream(fileUri);
			for (T t : list) {
				outputStream.writeObject(t);
			}
			outputStream.close();
		} catch (IOException e) {
			System.out.println("!!!\nGreska prilokom cuvanja fajla - " + fileUri);
		} finally {
			closeObjectOutputStream(outputStream);
		}
	}

	private File createFileIfNotExists(final String fileUri) throws IOException {
		File file = new File(fileUri);
		if (!file.exists()) {
			file.createNewFile();
		}
		return file;
	}

	private ObjectInputStream getObjectInputStream(final String fileUri) throws IOException {
		File file = createFileIfNotExists(fileUri);
		fileInputStream = new FileInputStream(file);
		ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
		return objectInputStream;
	}

	private ObjectOutputStream getObjectOutputStream(final String fileUri) throws IOException {
		File file = new File(fileUri);
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		return objectOutputStream;
	}

	private void closeObjectOutputStream(ObjectOutputStream outputStream) {
		if (outputStream != null) {
			try {
				outputStream.close();
			} catch (Exception e) {
			}
		}
	}

	private void closeObjectInputStream(ObjectInputStream inputStream) {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (Exception e) {
			}
		}
	}

	public static void saveLogedUserData(User user) {
		loggedUser = user;
	}

	public static User getLogedUserData() {
		return loggedUser;
	}
}
