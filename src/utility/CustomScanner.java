package utility;

import java.util.Scanner;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;

public class CustomScanner {

    private CustomScanner() {
    }

    public static String nextLine(String message) {
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String line = "";
        System.out.println(message);
        line = scanner.nextLine();
        return line;
    }

    public static int nextInt(String message) throws IOException {
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println(message);
            int choice = scanner.nextInt();
            return choice;
        } catch (Exception e) {
            scanner.hasNextLine();
            throw new IOException("Pogresan unos - unesite ceo broj.\n");
        }
    }

    public static float getFloat(String message) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        float output = sc.nextFloat();
        return output;
    }

    public static char getChar(String message) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        char output = sc.next().charAt(0);
        return output;
    }

    public static boolean getBoolean(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message + "Y/n");
        char answer = sc.next().charAt(0);
        return Character.toLowerCase(answer) != 'n';
    }

    public static Date getDate(String message) throws ParseException {

        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
        Date output = format.parse(sc.nextLine());
        return output;
    }

    public static char getChar(String[] messages) {
        Scanner sc = new Scanner(System.in);
        for (String message : messages) {
            System.out.println(message);
        }
        char output = sc.next().charAt(0);
        return output;
    }



    public static LocalTime getLocalTime(String message) throws ParseException {
        String time = CustomScanner.nextLine(message + " (HH:mm:ss): ");
        return LocalTime.parse(time);
    }

    public static Date getDateTime(String message) throws ParseException {
        String dateStr = CustomScanner.nextLine("Unesi datum (dd-MM-yyyy): ");
        String timeStr = CustomScanner.nextLine("Unesi vreme (HH-mm): ");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH-mm");
        try {
        	Date date = format.parse(dateStr + " " + timeStr);
        	return date;
        }catch (ParseException e) {
        	throw new ParseException("\nPogresan format datuma - vremena.", 0);
        }
    }
}
