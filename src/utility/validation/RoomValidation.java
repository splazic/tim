package utility.validation;

import javax.management.InvalidAttributeValueException;

import models.room.Room;

public class RoomValidation implements Validation<Room>{

	@Override
	public boolean isDataValid(Room room) throws InvalidAttributeValueException {
		StringBuilder message = new StringBuilder();
		message.append("");
		if(!room.getId().matches("[0-9]{2}-[0-9]{2}")) {
			message.append("\n[id] - id sobe treba da ima format 'XX-xx' (XX - broj sprata; xx - broj sobe)");
		}
		if(message.toString().equals(""))
			return true;
		throw new InvalidAttributeValueException("GRESKA!"+message.toString());
	}

}
