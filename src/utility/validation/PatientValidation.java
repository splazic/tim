package utility.validation;

import javax.management.InvalidAttributeValueException;

import models.user.User;
import models.user.patient.Patient;

public class PatientValidation implements Validation<Patient>{
	
	Validation<User> validation = new UserValidation();
	
	@Override
	public boolean isDataValid(Patient patient) throws InvalidAttributeValueException {
		StringBuilder message = new StringBuilder();
		message.append("");
		try {
			validation.isDataValid(patient);
		} catch (InvalidAttributeValueException e) {
			message.append(e.getMessage());
		}
		isMedicalNumberValid(patient.getMedicalNumber(), message);
		if(message.toString().equals("")) {
			return true;
		}
		throw new InvalidAttributeValueException("GRESKA!"+message);
	}
	
	private boolean isMedicalNumberValid(String medNumber, StringBuilder message) {
		if(medNumber.matches("^[0-9]{11}$")) {
			return true;
		}
		message.append("[Broj zdravstvene knjizice] - dozvoljen je unos brojeva (11 karaktera).");
		return false;
	}

}
