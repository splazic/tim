/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility.validation;

import javax.management.InvalidAttributeValueException;
import models.medical_record.MedicalRecordItem;

/**
 *
 * @author stefan
 */
public class MedicalRecordItemValidation implements Validation<MedicalRecordItem> {

    @Override
    public boolean isDataValid(MedicalRecordItem medicalRecordItem) throws InvalidAttributeValueException {
        StringBuilder message = new StringBuilder();
        message.append("");
        if (medicalRecordItem.getDescription().length() < 5) {
            message.append("Opis mora biti duzi od 5 karaktera");
        }
        if (message.toString().equals("")) {
            return true;
        }
        throw new InvalidAttributeValueException("GRESKA!" + message);
    }

}
