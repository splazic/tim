package utility.validation;

import javax.management.InvalidAttributeValueException;

import models.equipment.HospitalEquipment;

public class EquipmentValidation implements Validation<HospitalEquipment> {

	@Override
	public boolean isDataValid(HospitalEquipment equipment) throws InvalidAttributeValueException {
		StringBuilder message = new StringBuilder();
		message.append("");
		if(equipment.getTotalEquipment()<0) {
			message.append("[kolicina opreme] - ukupna kolicina opreme ne moze biti manja od 0");
		}
		if (!equipment.getName().matches("[a-zA-Z0-9 ]{3,25}")) {
			message.append("[naziv] - dozvoljen je unos slova, brojeva i praznih mesta (od 3 do 25 karaktera)");
		}
		if (message.toString().equals("")) {
			return true;
		}
		throw new InvalidAttributeValueException("GRESKA!" + message);
	}

}
