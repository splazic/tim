package utility.validation;

import javax.management.InvalidAttributeValueException;

public interface Validation<T> {
	boolean isDataValid(T object) throws InvalidAttributeValueException;
}
