package utility.validation;

import javax.management.InvalidAttributeValueException;

import models.user.User;

public class UserValidation implements Validation<User>{

	@Override
	public boolean isDataValid(User user) throws InvalidAttributeValueException{
		StringBuilder message = new StringBuilder();
		message.append("");
		isUsernameValid(user.getUsername(), message);
		isFirstNameValid(user.getFirstName(), message);
		isLastNameValid(user.getLastName(), message);
		isPasswordValid(user.getPassword(), message);
		if(message.toString().equals(""))
			return true;
		throw new InvalidAttributeValueException("GRESKA!"+message.toString());
	}
	
	
	private boolean isLastNameValid(String lastName, StringBuilder message) {
		if (lastName.matches("^[A-Za-z]{1,25}$")) {
			return true;
		}
		message.append("\n[Prezime] - dozvoljen je unos velikih i malih slova (od 1 do 25 karaktera).");
		return false;
	}
	
	private boolean isFirstNameValid(String firstName, StringBuilder message) {
		if (firstName.matches("^[A-Za-z]{1,25}$")) {
			return true;
		}
		message.append("\n[Ime] - dozvoljen je unos velikih i malih slova (od 1 do 25 karaktera).");
		return false;
	}
	
	private boolean isPasswordValid(String password, StringBuilder message) {
		if (password.matches("^.{8,25}$")) {
			return true;
		}
		message.append("\n[Lozinka] - treba imati izmedju 8 i 25 karaktera.");
		return false;
	}

	private boolean isUsernameValid(String username, StringBuilder message) {
		if (username.matches("^[a-z.0-9]{5,20}$")) {
			return true;
		}
		message.append("\n[Korisnicko ime] - dozvoljen je unos malih slova, brojeva i tacki (od 5 do 20 karaktera).");
		return false;
	}

}
