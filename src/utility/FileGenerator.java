/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author stefan
 */
public class FileGenerator {

    private FileGenerator() {
    }

    public static void generateCSV(List<String> colums, List<Object> data, String filePath) throws IOException {
        FileWriter csvWriter = new FileWriter(filePath);
        for (int i = 0; i < colums.size(); i++) {
            csvWriter.append(colums.get(i));
            if (i < colums.size() - 1) {
                csvWriter.append(",");
            }
        }
        csvWriter.append("\n");
        for (Object datum : data) {
            csvWriter.append(datum.toString());
            csvWriter.append("\n");
        }
     
        csvWriter.flush();
        csvWriter.close();
    }

    public static void generatePdf(List<String> colums, List<Object> data, String filePath) throws FileNotFoundException, DocumentException {
        Document doc = new Document();
        PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(filePath));
        doc.open();
        PdfPTable table = new PdfPTable(colums.size());
        addTableHeader(table, colums);
        addRows(table, data);
        doc.add(table);
        doc.close();
        doc.close();
        writer.close();
    }

    private static void addTableHeader(PdfPTable table, List<String> colums) {
        for (String column : colums) {
            PdfPCell header = new PdfPCell();
            header.setPhrase(new Phrase(column));
            table.addCell(header);
        }
    }

    private static void addRows(PdfPTable table, List<Object> data) {
        for (Object datum : data) {
            String[] values = datum.toString().split(",");
            for (String value : values) {
                table.addCell(value);
            }
        }
    }

    public static String generateFileName() {
        return System.getProperty("user.dir") + "/";
    }
}
