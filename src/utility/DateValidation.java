package utility;

import java.time.LocalTime;
import java.util.Date;

import dto.date.DateDTO;

public class DateValidation {
	
	private static DateValidation dateValidation;
	
	private DateValidation() {
		// TODO Auto-generated constructor stub
	}
	
	public static DateValidation getInstance() {
		if (dateValidation == null) {
			dateValidation = new DateValidation();
		}
		return dateValidation;
	}
	
	public boolean isDateValid(Date start, Date end) {
		Date currentDate = new Date();
		if (start.before(currentDate) || start.equals(currentDate)) {
			if (end == null || end.after(currentDate)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isDateOverlap(DateDTO date1, DateDTO date2) {
		Date d1StartD = date1.getStartDate();
		Date d1EndD = date1.getEndDate();
		Date d2StartD = date2.getStartDate();
		Date d2EndD = date2.getEndDate();
		if((d1StartD.equals(d2StartD) || d1StartD.after(d2StartD)) && d1StartD.before(d2EndD)){
			return true;
		}else if(d1EndD.after(d2StartD) && (d1EndD.equals(d2EndD) || d1EndD.before(d2EndD))){
			return true;
		}else if(d1StartD.before(d2StartD)  && d1EndD.after(d2EndD)) {
			return true;
		}
		return false;
	}
	
	public boolean isDateOverlap(Date startDate, Date endDate) {
		Date currentDate = new Date();
		if((startDate.equals(currentDate) || startDate.before(currentDate)) && endDate.after(currentDate)) {
			return true;
		}
		return false;
	}
	
	public boolean isTimeOverlap(LocalTime startTime, LocalTime endTime) {
		LocalTime currentTime = LocalTime.now();
		if((startTime.equals(currentTime) || startTime.isBefore(currentTime)) && endTime.isAfter(currentTime)) {
			return true;
		}
		return false;
	}
}
