package javaapplication;

import menu.menu.Menu;

public class Main {
	public static void main(String[] args) {
        Menu menu = Menu.getInstance();
        menu.startMenu();
        System.exit(0);
	}
}
