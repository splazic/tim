package service.crud;

import java.util.List;

import javax.management.InvalidAttributeValueException;
import exceptions.ResourceNotFound;
import models.Entity;
import repository.generic.GenericRepository;

public class CRUDService<T extends Entity<ID>, ID, REPO extends GenericRepository<T, ID>> {
	
	private REPO repo;

	public CRUDService(REPO repo) {
		this.repo = repo;
	}

	public T getById(ID id) throws ResourceNotFound {
		return repo.getById(id);
	}

	public List<T> getAll() {
		return repo.getAll();
	}

	public T update(T object) throws ResourceNotFound, InvalidAttributeValueException {
		return repo.update(object);
	}

	public T delete(T object) throws ResourceNotFound {
		return repo.delete(object);
	}

	public T save(T object) throws InvalidAttributeValueException{
		return repo.save(object);
	}

}
