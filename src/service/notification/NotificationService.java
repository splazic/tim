package service.notification;

import models.notification.Notification;
import repository.notification.NotificationRepository;
import service.crud.CRUDService;

public class NotificationService extends CRUDService<Notification, Integer, NotificationRepository> implements INotificationService{

	public NotificationService(NotificationRepository repo) {
		super(repo);
	}

}
