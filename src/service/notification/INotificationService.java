package service.notification;

import models.notification.Notification;
import repository.generic.GenericRepository;

public interface INotificationService extends GenericRepository<Notification, Integer>{

}
