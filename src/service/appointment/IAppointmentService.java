package service.appointment;

import dto.appointment.AppointmentDTO;
import dto.appointment.PriorityTypeDTO;
import dto.report.DoctorScheduleDTO;
import exceptions.ResourceNotFound;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.management.InvalidAttributeValueException;

import models.appointment.Appointment;
import models.appointment.TimeSlot;
import models.notification.Notification;
import models.room.Room;
import models.user.doctor.Doctor;
import models.user.patient.Patient;
import repository.generic.GenericRepository;

public interface IAppointmentService extends GenericRepository<Appointment, Integer>{
    List<Appointment> getAllByRoomId(String id);
    List<Appointment> getAllScheduledAppointments();
    boolean isAppointmentFree(Room room);
    List<AppointmentDTO> getAllByPatient(Integer id) throws ResourceNotFound;
    public List<AppointmentDTO> getActiveByPatient(Integer id) throws ResourceNotFound;
    public List<AppointmentDTO> getActiveByDoctor(Integer id) throws ResourceNotFound;
    public DoctorScheduleDTO getDoctorReport(Integer doctorId) throws ResourceNotFound;
    public List<Object> getPatientsByDoctor(Integer doctorId) throws ResourceNotFound;
    public Appointment createOperation(TimeSlot slot, Date date, String roomId, Patient patient, Doctor doctor, boolean isUregent) throws InvalidAttributeValueException, ResourceNotFound;
    public Notification scheduleAppointment(Doctor doctor, Patient patient, Date startDate, Date endDate) throws InvalidAttributeValueException;
    public Appointment createAppointment(Doctor doctor, TimeSlot slot, LocalDate date, Integer medicalTypeAppointment, PriorityTypeDTO priorityTypeDTO, Patient patient) throws ResourceNotFound,InvalidAttributeValueException;
    
}
