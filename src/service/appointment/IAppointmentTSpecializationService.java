package service.appointment;

import models.appointment.AppointmentTypeSpecialization;
import repository.generic.GenericRepository;

public interface IAppointmentTSpecializationService extends GenericRepository<AppointmentTypeSpecialization, Integer>{

}
