package service.appointment;

import models.appointment.AppointmentTypeSpecialization;
import repository.appointment.appointment_type_specialization.AppointmentTSpecializationRepository;
import service.crud.CRUDService;

public class AppointmentTSpecializationService extends CRUDService<AppointmentTypeSpecialization, Integer, AppointmentTSpecializationRepository> implements IAppointmentTSpecializationService{

	public AppointmentTSpecializationService(AppointmentTSpecializationRepository repo) {
		super(repo);
	}

}
