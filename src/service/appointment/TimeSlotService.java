package service.appointment;

import models.appointment.TimeSlot;
import repository.appointment.time_slot.TimeSlotRepository;
import service.crud.CRUDService;

public class TimeSlotService extends CRUDService<TimeSlot, Integer, TimeSlotRepository> implements ITimeSlotService {

    public TimeSlotService(TimeSlotRepository repo) {
        super(repo);
    }

}
