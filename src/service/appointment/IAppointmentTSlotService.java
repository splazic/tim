package service.appointment;

import models.appointment.AppointmentTimeSlot;
import repository.generic.GenericRepository;

public interface IAppointmentTSlotService extends GenericRepository<AppointmentTimeSlot, Integer> {

}
