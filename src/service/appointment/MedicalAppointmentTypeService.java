package service.appointment;

import exceptions.ResourceNotFound;
import models.appointment.type.AppointmentType;
import models.appointment.type.MedicalAppointmentType;
import repository.appointment.appointment_type.MedicalAppointmentTypeRepository;
import service.crud.CRUDService;

public class MedicalAppointmentTypeService extends CRUDService<MedicalAppointmentType, Integer, MedicalAppointmentTypeRepository> implements IMedicalAppointmentTypeService {

    public MedicalAppointmentTypeService(MedicalAppointmentTypeRepository repo) {
        super(repo);
    }

    @Override
    public MedicalAppointmentType getById(Integer id) throws ResourceNotFound {
        return super.getById(id);
    }

    public MedicalAppointmentType getByAppointmentType(AppointmentType appointmentType) throws ResourceNotFound {

        for (MedicalAppointmentType medicalAppointmentType : super.getAll()) {
            if (medicalAppointmentType.getAppointmentType() == appointmentType) {
                return medicalAppointmentType;
            }
        }
        throw new ResourceNotFound("Nema datog tipa");
    }

}
