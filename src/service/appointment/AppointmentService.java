package service.appointment;

import java.time.LocalDate;
import dto.appointment.AppointmentDTO;
import dto.appointment.PriorityTypeDTO;
import dto.report.DoctorScheduleDTO;
import dto.user.UserDTO;
import exceptions.ResourceNotFound;
import java.time.Duration;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.management.InvalidAttributeValueException;

import models.appointment.Appointment;
import models.appointment.AppointmentStatus;
import models.appointment.type.MedicalAppointmentType;
import models.appointment.AppointmentTimeSlot;
import models.appointment.type.AppointmentType;

import models.appointment.TimeSlot;
import models.notification.MessageNotification;
import models.notification.Notification;
import models.notification.ScheduleNotification;
import models.room.Room;
import models.user.User;
import models.user.doctor.Doctor;
import models.user.patient.Patient;
import repository.appointment.appointment.AppointmentRepository;
import repository.appointment.appointment_time_slot.AppointmentTimeSlotRepository;
import repository.appointment.appointment_type.MedicalAppointmentTypeRepository;
import repository.appointment.time_slot.TimeSlotRepository;
import repository.notification.NotificationRepository;
import repository.room.room.RoomRepository;
import repository.user.user.UserRepository;
import service.crud.CRUDService;

public class AppointmentService extends CRUDService<Appointment, Integer, AppointmentRepository>
        implements IAppointmentService {

    private AppointmentRepository repo;
    private RoomRepository roomRepository;
    private MedicalAppointmentTypeRepository medicalAppointmentTypeRepository;
    private TimeSlotRepository timeSlotRepository;
    private AppointmentTimeSlotRepository appointmentTimeSlotRepository;
    private UserRepository userRepository;
    private NotificationRepository notificationRepository;

    public AppointmentService(AppointmentRepository repo, UserRepository userRepository, RoomRepository roomRepository, MedicalAppointmentTypeRepository medicalAppointmentTypeRepository, TimeSlotRepository timeSlotRepository, AppointmentTimeSlotRepository appointmentTimeSlotRepository, NotificationRepository notificationRepository) {
        super(repo);
        this.repo = repo;
        this.roomRepository = roomRepository;
        this.medicalAppointmentTypeRepository = medicalAppointmentTypeRepository;
        this.timeSlotRepository = timeSlotRepository;
        this.appointmentTimeSlotRepository = appointmentTimeSlotRepository;
        this.userRepository = userRepository;
        this.notificationRepository = notificationRepository;
    }

    @Override
    public boolean isAppointmentFree(Room room) {
        for (Appointment appointment : this.repo.getAll()) {
            if (appointment.getRoomId().equals(room.getId())) {
                if (appointment.getAppointmentStatus() == AppointmentStatus.SCHEDULED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public List<Appointment> getAllScheduledAppointments() {
        List<Appointment> list = new ArrayList<Appointment>();
        LocalDate currentDate = LocalDate.now();
        for (Appointment appointment : getAll()) {
            if (!appointment.isDeleted()) {
                if (appointment.getAppointmentStatus() == AppointmentStatus.SCHEDULED) {
                    if (appointment.getDate().isAfter(currentDate)) {
                        list.add(appointment);
                    }
                }
            }
        }
        return list;
    }

    @Override
    public List<Appointment> getAllByRoomId(String id) {
        List<Appointment> list = new ArrayList<Appointment>();
        for (Appointment appointment : getAll()) {
            if (!appointment.isDeleted() && appointment.getRoomId().equals(id)) {
                list.add(appointment);
            }
        }
        return list;
    }

    @Override
    public List<AppointmentDTO> getAllByPatient(Integer id) throws ResourceNotFound {
        List<AppointmentDTO> appointmentDTOs = new ArrayList<>();
        for (Appointment appointment : super.getAll()) {
            if (appointment.getUserId() == id) {
                MedicalAppointmentType appointmentType = medicalAppointmentTypeRepository.getById(appointment.getMedAppointmentTypeId());
                User user = userRepository.getById(appointment.getUserId());
                User patient = userRepository.getById(appointment.getDoctorId());
                AppointmentDTO adto = new AppointmentDTO(appointment, user, patient, appointmentType);
                appointmentDTOs.add(adto);
            }
        }
        return appointmentDTOs;
    }

    @Override
    public List<AppointmentDTO> getActiveByPatient(Integer id) throws ResourceNotFound {
        List<AppointmentDTO> appointmentDTOs = new ArrayList<>();
        LocalDate deadlineDate = LocalDate.now().plusDays(2);
        for (Appointment appointment : super.getAll()) {
            if (appointment.getUserId() == id) {
                if (appointment.getAppointmentStatus() == AppointmentStatus.SCHEDULED && appointment.getDate().isBefore(deadlineDate)) {
                    MedicalAppointmentType appointmentType = medicalAppointmentTypeRepository.getById(appointment.getMedAppointmentTypeId());
                    User user = userRepository.getById(appointment.getUserId());
                    User patient = userRepository.getById(appointment.getDoctorId());
                    AppointmentDTO adto = new AppointmentDTO(appointment, user, patient, appointmentType);
                    appointmentDTOs.add(adto);
                }
            }
        }
        return appointmentDTOs;
    }

    @Override
    public List<AppointmentDTO> getActiveByDoctor(Integer id) throws ResourceNotFound {
        List<AppointmentDTO> appointmentDTOs = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        for (Appointment appointment : getAll()) {
            if (!appointment.isDeleted()) {
                if (appointment.getAppointmentStatus() == AppointmentStatus.SCHEDULED && appointment.getDoctorId() == id) {
                    if (appointment.getDate().isAfter(currentDate) || appointment.getDate().isEqual(currentDate)) {
                        MedicalAppointmentType appointmentType = medicalAppointmentTypeRepository.getById(appointment.getMedAppointmentTypeId());
                        User user = userRepository.getById(appointment.getDoctorId());
                        User patient = userRepository.getById(appointment.getDoctorId());
                        AppointmentDTO adto = new AppointmentDTO(appointment, user, patient, appointmentType);
                        appointmentDTOs.add(adto);
                    }
                }
            }
        }
        return appointmentDTOs;
    }

    @Override
    public DoctorScheduleDTO getDoctorReport(Integer doctorId) throws ResourceNotFound {
        float scheduleHours = 0;
        List<TimeSlot> slots;
        for (Appointment appointment : super.getAll()) {
            if (appointment.getDoctorId() == doctorId) {
                slots = getByDoctor(doctorId);
                for (TimeSlot slot : slots) {
                    scheduleHours = Duration.between(slot.getStartTime(), slot.getEndTime()).toHours();
                }
            }
        }
        return new DoctorScheduleDTO(scheduleHours);
    }

    @Override
    public List<Object> getPatientsByDoctor(Integer doctorId) throws ResourceNotFound {
        List<Object> users = new ArrayList<>();
        for (Appointment appointment : super.getAll()) {
            if (appointment.getDoctorId() == doctorId) {
                User user = userRepository.getById(appointment.getUserId());
                UserDTO userDTO = new UserDTO(user);
                if (!users.contains(userDTO)) {
                    users.add(userDTO);
                }
            }
        }
        return users;
    }

    @Override
    public Appointment createOperation(TimeSlot slot, Date date, String roomId, Patient patient, Doctor doctor, boolean isUregent) throws InvalidAttributeValueException, ResourceNotFound {
        roomRepository.getById(roomId);
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        isRoomFree(roomId, localDate, slot);
        isPatientFree(patient.getId(), localDate, slot);
        isDoctorFree(doctor.getId(), localDate, slot);
        Appointment appointment = new Appointment(patient.getId(), doctor.getId(), roomId, localDate, AppointmentStatus.SCHEDULED, 0);
        slot = timeSlotRepository.save(slot);
        AppointmentTimeSlot appointmentTimeSlot = new AppointmentTimeSlot(appointment.getId(), slot.getId());
        appointmentTimeSlotRepository.save(appointmentTimeSlot);
        appointment = super.save(appointment);
        if (isUregent) {
            Notification notification = new MessageNotification(appointment.getId(), "Zakazan pregled");
            notificationRepository.save(notification);
        }
        return appointment;
    }

    @Override
    public Notification scheduleAppointment(Doctor doctor, Patient patient, Date startDate, Date endDate) throws InvalidAttributeValueException {
        LocalDate startD = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endD = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        //cretate nofitication type
        Notification not = new ScheduleNotification(doctor.getId(), patient.getId(), startD, endD);
        return notificationRepository.save(not);
    }

    private boolean isDoctorFree(Integer doctorId, LocalDate date, TimeSlot slot) throws ResourceNotFound {
        for (Appointment appointment : super.getAll()) {
            if (appointment.getDoctorId() == (doctorId)) {
                if (appointment.getDate().compareTo(date) == 0) {
                    if (!isSlotFree(appointment.getId(), slot)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean isPatientFree(Integer patientId, LocalDate date, TimeSlot slot) throws ResourceNotFound {
        for (Appointment appointment : super.getAll()) {
            if (appointment.getUserId() == (patientId)) {
                if (appointment.getDate().compareTo(date) == 0) {
                    if (!isSlotFree(appointment.getId(), slot)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean isRoomFree(String roomId, LocalDate date, TimeSlot slot) throws ResourceNotFound {
        for (Appointment appointment : super.getAll()) {
            if (appointment.getRoomId().equalsIgnoreCase(roomId)) {
                if (appointment.getDate().compareTo(date) == 0) {
                    if (!isSlotFree(appointment.getId(), slot)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public List<TimeSlot> getByDoctor(Integer appointmentId) throws ResourceNotFound {
        List<TimeSlot> slots = new ArrayList<>();
        for (AppointmentTimeSlot appointmentTimeSlot : this.appointmentTimeSlotRepository.getAll()) {
            if (appointmentTimeSlot.getAppointmentId() == appointmentId) {
                TimeSlot slot = this.timeSlotRepository.getById(appointmentTimeSlot.getTimeSlotId());
                slots.add(slot);
            }
        }
        return slots;
    }

    public boolean isSlotFree(Integer appointmentId, TimeSlot slot) throws ResourceNotFound {
        for (AppointmentTimeSlot appointmentTimeSlot : this.appointmentTimeSlotRepository.getAll()) {
            if (appointmentTimeSlot.getAppointmentId() == appointmentId) {
                TimeSlot timeSlot = this.timeSlotRepository.getById(appointmentTimeSlot.getTimeSlotId());
                if (timeSlot.getStartTime().compareTo(slot.getStartTime()) == 0 && timeSlot.getEndTime().compareTo(slot.getEndTime()) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public Appointment createAppointment(Doctor doctor, TimeSlot slot, LocalDate date, Integer medicalTypeAppointment, PriorityTypeDTO priorityTypeDTO, Patient patient) throws ResourceNotFound, InvalidAttributeValueException {
        //slucaj kada postoji lekar sa slobodnim slotom

        for (LocalDate currentDate = LocalDate.now(); currentDate.isBefore(date); currentDate = currentDate.plusDays(1)) {
            if (isDoctorFree(doctor.getId(), currentDate, slot) && isPatientFree(patient.getId(), currentDate, slot)) {
                //rezervisi appointment
                Room room = findRoom(slot, date);
                Appointment appointment = new Appointment(patient.getId(), doctor.getId(), room.getId(), currentDate, AppointmentStatus.SCHEDULED, medicalTypeAppointment);
                return super.save(appointment);
            }
        }
        //nadji drugi appointment
        if (priorityTypeDTO == PriorityTypeDTO.DOCTOR) {
            for (LocalDate currentDate = LocalDate.now(); currentDate.isBefore(date); currentDate = currentDate.plusDays(1)) {
                TimeSlot sl = generateSlots(doctor, patient, currentDate);
                if (sl != null) {
                    Room room = findRoom(sl, date);
                    Appointment appointment = new Appointment(patient.getId(), doctor.getId(), room.getId(), currentDate, AppointmentStatus.SCHEDULED, medicalTypeAppointment);
                    return super.save(appointment);
                }

            }
            throw new ResourceNotFound("Ne moze pronaci odgovarajuci datum");

        } else if (priorityTypeDTO == PriorityTypeDTO.DATE) {
            //pronadji bilo sta jos blize
            throw new ResourceNotFound("Ne radi");
        } else {
            //ponadji drugog doktora na istom timeslotu
            throw new ResourceNotFound("Ne radi");
        }

    }

    private Room findRoom(TimeSlot slot, LocalDate date) throws ResourceNotFound {
        for (Room room : roomRepository.getAll()) {
            if (isRoomFree(room.getId(), date, slot)) {
                return room;
            }
        }
        throw new ResourceNotFound("Nema slobodne sobe");
    }

    private TimeSlot generateSlots(Doctor doctor, Patient patient, LocalDate currentDate) throws ResourceNotFound {
        LocalTime localTime = LocalTime.parse("00:00:00");
        for (LocalTime currentTime = LocalTime.now(); currentTime.isBefore(localTime); currentTime = currentTime.plusMinutes(30)) {
            TimeSlot slot = new TimeSlot(currentTime, currentTime.plusMinutes(30));
            if (isDoctorFree(doctor.getId(), currentDate, slot) && isPatientFree(patient.getId(), currentDate, slot)) {
                return slot;
            }

        }
        return null;
    }

}
