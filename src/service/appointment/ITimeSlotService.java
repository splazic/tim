package service.appointment;

import exceptions.ResourceNotFound;
import models.appointment.TimeSlot;
import repository.generic.GenericRepository;

public interface ITimeSlotService extends GenericRepository<TimeSlot, Integer> {

    public TimeSlot getById(Integer id) throws ResourceNotFound;
}
