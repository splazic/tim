package service.appointment;

import exceptions.ResourceNotFound;
import models.appointment.type.AppointmentType;
import models.appointment.type.MedicalAppointmentType;
import repository.generic.GenericRepository;

public interface IMedicalAppointmentTypeService extends GenericRepository<MedicalAppointmentType, Integer> {

    public MedicalAppointmentType getById(Integer id) throws ResourceNotFound;
    public MedicalAppointmentType getByAppointmentType(AppointmentType appointmentType) throws ResourceNotFound;
}
