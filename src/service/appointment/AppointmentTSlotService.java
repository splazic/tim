package service.appointment;

import models.appointment.AppointmentTimeSlot;
import repository.appointment.appointment_time_slot.AppointmentTimeSlotRepository;
import repository.appointment.time_slot.TimeSlotFileRepo;
import service.crud.CRUDService;
import utility.FileManager;

public class AppointmentTSlotService extends CRUDService<AppointmentTimeSlot, Integer, AppointmentTimeSlotRepository> implements IAppointmentTSlotService {

    private ITimeSlotService timeSlotService = new TimeSlotService(new TimeSlotFileRepo(FileManager.TIME_SLOT_URI));

    public AppointmentTSlotService(AppointmentTimeSlotRepository repo) {
        super(repo);
    }
}
