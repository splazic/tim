package service.user;

import models.user.Secretary;
import repository.generic.GenericRepository;

public interface ISecretaryService extends GenericRepository<Secretary, Integer>{

}
