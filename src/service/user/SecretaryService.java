package service.user;

import models.user.Secretary;
import repository.user.secretary.SecretaryRepository;
import service.crud.CRUDService;

public class SecretaryService extends CRUDService<Secretary, Integer, SecretaryRepository> implements ISecretaryService{

	public SecretaryService(SecretaryRepository repo) {
		super(repo);
	}

}
