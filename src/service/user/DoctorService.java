package service.user;

import javax.management.InvalidAttributeValueException;

import models.user.User;
import models.user.doctor.Doctor;
import repository.user.doctor.DoctorRepository;
import service.crud.CRUDService;
import utility.validation.UserValidation;
import utility.validation.Validation;

public class DoctorService extends CRUDService<Doctor, Integer, DoctorRepository> implements IDoctorService{

	Validation<User> validation = new UserValidation();
	
	public DoctorService(DoctorRepository repo) {
		super(repo);
	}
	
	@Override
	public Doctor save(Doctor doctor) throws InvalidAttributeValueException {
		validation.isDataValid(doctor);
		return super.save(doctor);
	}
	
}
