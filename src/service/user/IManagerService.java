package service.user;

import models.user.Manager;
import repository.generic.GenericRepository;

public interface IManagerService extends GenericRepository<Manager, Integer>{

}
