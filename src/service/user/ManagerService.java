package service.user;

import models.user.Manager;
import repository.user.manager.ManagerRepository;
import service.crud.CRUDService;

public class ManagerService extends CRUDService<Manager, Integer, ManagerRepository> implements IManagerService{

	public ManagerService(ManagerRepository repo) {
		super(repo);
	}

}
