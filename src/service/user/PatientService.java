package service.user;

import javax.management.InvalidAttributeValueException;
import models.user.patient.Patient;
import repository.user.patient.PatientRepository;
import service.crud.CRUDService;
import utility.validation.PatientValidation;
import utility.validation.Validation;

public class PatientService extends CRUDService<Patient, Integer, PatientRepository> implements IPatientService{

	Validation<Patient> validation = new PatientValidation();
	
	public PatientService(PatientRepository repo) {
		super(repo);
	}

    @Override
    public Patient save(Patient object) throws InvalidAttributeValueException {
        this.validation.isDataValid(object);
        return super.save(object); //To change body of generated methods, choose Tools | Templates.
    }
        
        
}
