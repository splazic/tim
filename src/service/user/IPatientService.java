package service.user;

import javax.management.InvalidAttributeValueException;
import models.user.patient.Patient;
import repository.generic.GenericRepository;

public interface IPatientService extends GenericRepository<Patient, Integer> {

    public Patient save(Patient object) throws InvalidAttributeValueException;

}
