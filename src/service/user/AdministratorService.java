package service.user;

import models.user.Administrator;
import repository.user.admin.AdministratorRepository;
import service.crud.CRUDService;

public class AdministratorService extends CRUDService<Administrator, Integer, AdministratorRepository> implements IAdministratorService{

	public AdministratorService(AdministratorRepository repo) {
		super(repo);
	}
}
