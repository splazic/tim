package service.user;

import models.user.doctor.Doctor;
import repository.generic.GenericRepository;

public interface IDoctorService extends GenericRepository<Doctor, Integer>{

}
