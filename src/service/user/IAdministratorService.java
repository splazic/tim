package service.user;

import models.user.Administrator;
import repository.generic.GenericRepository;

public interface IAdministratorService extends GenericRepository<Administrator, Integer>{

}
