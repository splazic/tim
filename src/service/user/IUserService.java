package service.user;

import exceptions.ResourceNotFound;
import java.util.List;

import javax.security.auth.login.CredentialNotFoundException;

import models.user.User;
import repository.generic.GenericRepository;

public interface IUserService extends GenericRepository<User, Integer> {
	public User checkCredentials(final String userName, final String password) throws CredentialNotFoundException;

	public User getByUsername(String username) throws ResourceNotFound;

	public User delete(User object) throws ResourceNotFound;

	public List<User> getActiveUserListWithoutAdmin();
}
