package service.user;

import javax.management.InvalidAttributeValueException;
import javax.security.auth.login.CredentialNotFoundException;

import exceptions.ResourceNotFound;
import java.util.ArrayList;
import java.util.List;
import models.user.Administrator;
import models.user.User;
import models.user.doctor.Doctor;
import models.user.patient.Patient;
import repository.user.user.UserRepository;
import service.crud.CRUDService;
import utility.validation.UserValidation;
import utility.validation.Validation;

public class UserService extends CRUDService<User, Integer, UserRepository> implements IUserService {

	private UserRepository repo;
	private Validation<User> validation = new UserValidation();

	public UserService(UserRepository repo) {
		super(repo);
		this.repo = repo;
	}

	public User getByUsername(String username) throws ResourceNotFound {
		return repo.getByUsername(username);
	}

	@Override
	public User update(User user) throws ResourceNotFound, InvalidAttributeValueException {
		User u = getById(user.getId());
		if (!u.getUsername().equals(user.getUsername()))
			isUsernameUnique(user.getUsername());
		validation.isDataValid(user);
		return super.update(user);
	}

	@Override
	public User save(User user) throws InvalidAttributeValueException {
		isUsernameUnique(user.getUsername());
		validation.isDataValid(user);
		return super.save(user);
	}

	private boolean isUsernameUnique(String username) throws InvalidAttributeValueException {
		try {
			getByUsername(username);
			throw new InvalidAttributeValueException(
					"GRESKA!\n[Korisnicko ime] - '" + username + "' nije jedinstveno.");
		} catch (ResourceNotFound e) {
			return true;
		}
	}

	public User checkCredentials(final String userName, final String password) throws CredentialNotFoundException {
		try {
			User user = getByUsername(userName);
			if (user != null) {
				if (user.getPassword().equals(password) && !user.isDeleted())
					return user;
			}
		} catch (Exception e) {
		}
		throw new CredentialNotFoundException("\nPogresno korisnicko ime - lozinka.");
	}

	@Override
	public User delete(User user) throws ResourceNotFound {
		if (user instanceof Patient) {
			// check if patient has some appointments
		}
		// if user is a doctor - you cant delete him until he has no appointments
		if (user instanceof Doctor) {
			// check if user is appointment free
		}
		return repo.delete(user);
	}

	@Override
	public List<User> getActiveUserListWithoutAdmin() {
		List<User> users = new ArrayList<>();
		for (User user : repo.getAll()) {
			if (!(user instanceof Administrator) && !user.isDeleted()) {
				users.add(user);
			}
		}
		return users;
	}

}
