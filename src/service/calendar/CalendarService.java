package service.calendar;

import models.calendar.Calendar;
import repository.calendar.CalendarRepository;
import service.crud.CRUDService;

public class CalendarService extends CRUDService<Calendar, Integer, CalendarRepository> implements ICalendarService{

	public CalendarService(CalendarRepository repo) {
		super(repo);
	}

}
