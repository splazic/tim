package service.calendar;

import models.calendar.Calendar;
import repository.generic.GenericRepository;

public interface ICalendarService extends GenericRepository<Calendar, Integer>{

}
