package service.renovation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.management.InvalidAttributeValueException;

import dto.renovation.RenovationDTO;
import exceptions.ResourceNotFound;
import models.renovation.Renovation;
import models.renovation.RenovationRoom;
import models.renovation.RenovationType;
import repository.renovation.renovation.RenovationRepository;
import service.crud.CRUDService;

public class RenovationService extends CRUDService<Renovation, Integer, RenovationRepository>
		implements IRenovationService {

	private IRenovationRoomService renRoomService = null;

	
	public RenovationService(RenovationRepository repo) {
		super(repo);
	}

	public void setRenovationRoomService (IRenovationRoomService renovationRoomService) {
		if(this.renRoomService == null) 
			this.renRoomService = renovationRoomService;
	}
	
	@Override
	public Renovation save(Renovation renovation) throws InvalidAttributeValueException {
		//uraditi validaciju (tip, datum, user)
		if(renovation.getRenovationType() == RenovationType.UPDATE_ROOM_TYPE) {
			addRoomTypeRenovation();
		}
		return super.save(renovation);
	}
	
	private void addRoomTypeRenovation() {
		
		//provjeri da li vec postoji zakazano renoviranje za sobu
		//provjeri da li postoje zakazani pregledi-operacije
	}
	
	@Override
	public List<RenovationDTO> getAllByRoomId(String id) {
		List<RenovationDTO> list = new ArrayList<RenovationDTO>();
		for (Renovation renovation : super.getAll()) {
			if (!renovation.isDeleted()) {
				List<RenovationRoom> renRoomList = renRoomService.getAllByRenovationId(renovation.getId());
				for (RenovationRoom renRoom : renRoomList) {
					if (renRoom.getRoomId().equals(id)) {
						list.add(new RenovationDTO(renovation, renRoomList));
						break;
					}
				}
			}
		}
		return list;
	}

	private boolean isDateValid(Date start, Date end) {
		Date currentDate = new Date();
		if (start.equals(currentDate) || start.before(currentDate)) {
			if (end.after(currentDate)) {
				return true;
			}
		}
		return false;
	}

	public boolean isRoomInRenovation(String roomId) {
		List<RenovationDTO> renList = getAllByRoomId(roomId);
		for (RenovationDTO renovationDTO : renList) {
			Date renStart = renovationDTO.getRenovation().getRenovationStartDate();
			Date renEnd = renovationDTO.getRenovation().getRenovatinoEndDate();
			if (isDateValid(renStart, renEnd)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<RenovationDTO> getScheduledRenovation(String roomId) {
		List<RenovationDTO> list = new ArrayList<RenovationDTO>();
		Date currentDate = new Date();
		for (RenovationDTO renovationDTO : getAllByRoomId(roomId)) {
			if (renovationDTO.getRenovation().getRenovationStartDate().after(currentDate)) {
				list.add(renovationDTO);
			}
		}
		return list;
	}

	@Override
	public List<RenovationDTO> getScheduledRenovation() {
		List<RenovationDTO> list = new ArrayList<RenovationDTO>();
		Date currentDate = new Date();
		for (Renovation renovation : getAll()) {
			if (!renovation.isDeleted() && renovation.getRenovationStartDate().after(currentDate)) {
				list.add(new RenovationDTO(renovation, renRoomService.getAllByRenovationId(renovation.getId())));
			}
		}
		return list;
	}

	@Override
	public boolean checkFinished(Integer id) throws ResourceNotFound {
		Renovation renovation = getById(id);
		Date currentDate = new Date();
		return renovation.getRenovatinoEndDate().compareTo(currentDate) < 0;
	}

}
