package service.renovation;

import java.util.List;

import dto.renovation.RenovationDTO;
import exceptions.ResourceNotFound;
import models.renovation.Renovation;
import repository.generic.GenericRepository;

public interface IRenovationService extends GenericRepository<Renovation, Integer>{
	List<RenovationDTO> getAllByRoomId(String id);
	boolean isRoomInRenovation(String roomId);
	public List<RenovationDTO> getScheduledRenovation(String roomId);
	public List<RenovationDTO> getScheduledRenovation(); 
	public boolean checkFinished(Integer id) throws ResourceNotFound;
}