package service.renovation;

import java.util.List;

import exceptions.ResourceNotFound;
import models.renovation.RenovationRoom;
import models.room.Room;
import repository.generic.GenericRepository;

public interface IRenovationRoomService extends GenericRepository<RenovationRoom, Integer>{
	List<RenovationRoom> getAllByRenovationId(int id);
    boolean isRenovationFree(Room room) throws ResourceNotFound;
}
