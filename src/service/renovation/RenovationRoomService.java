package service.renovation;

import java.util.ArrayList;
import java.util.List;

import exceptions.ResourceNotFound;
import models.renovation.RenovationRoom;
import models.room.Room;
import repository.renovation.renovation_room.RenovationRoomRepository;
import service.crud.CRUDService;

public class RenovationRoomService extends CRUDService<RenovationRoom, Integer, RenovationRoomRepository>
		implements IRenovationRoomService {

	private RenovationRoomRepository repo;

	public RenovationRoomService(RenovationRoomRepository repo) {
		super(repo);
		this.repo = repo;
	}

	@Override
	public boolean isRenovationFree(Room room) throws ResourceNotFound {
		for (RenovationRoom renovationRoom : this.repo.getAll()) {
			//if (!renovationService.checkFinished(renovationRoom.getRenovationId())) {
				return false;
			//}
		}
		return true;
	}

	@Override
	public List<RenovationRoom> getAllByRenovationId(int renovationId) {
		List<RenovationRoom> list = new ArrayList<RenovationRoom>();
		for (RenovationRoom renRoom : getAll()) {
			if (!renRoom.isDeleted()) {
				if (renRoom.getRenovationId() == renovationId) {
					list.add(renRoom);
				}
			}
		}
		return list;
	}

}
