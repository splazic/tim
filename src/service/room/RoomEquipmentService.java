package service.room;

import models.room.RoomHospitalEquipment;
import repository.room.room_equipment.RoomEquipmentRepository;
import service.crud.CRUDService;

public class RoomEquipmentService extends CRUDService<RoomHospitalEquipment, Integer, RoomEquipmentRepository>
		implements IRoomEquipmentService {
	
	public RoomEquipmentService(RoomEquipmentRepository repo) {
		super(repo);
	}
}
