package service.room;

import dto.date.DateDTO;
import dto.room.RoomDTO;
import exceptions.ResourceNotFound;
import java.util.List;
import javax.management.InvalidAttributeValueException;
import models.room.Room;
import models.room.RoomType;
import models.user.User;
import repository.generic.GenericRepository;

public interface IRoomService extends GenericRepository<Room, String> {
	public RoomDTO getRoomWithEquipment(String id) throws ResourceNotFound;

	public Room save(Room object) throws InvalidAttributeValueException;

	public List<Room> getAll();

	public RoomType[] getRoomTypes();

	public Room updateRoom(RoomDTO uRoom, DateDTO renovationDate, User user)
			throws ResourceNotFound, InvalidAttributeValueException;

	public Room mergeRooms(User user, Room room1, Room room2, DateDTO date) throws ResourceNotFound;

	public Room splitRoom(User user, Room room1, Room room2, DateDTO date) throws ResourceNotFound;

}
