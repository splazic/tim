package service.room;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dto.date.DateDTO;
import dto.room.RoomDTO;
import dto.room.RoomEquDTO;
import exceptions.ResourceNotFound;
import javax.management.InvalidAttributeValueException;

import models.appointment.Appointment;
import models.appointment.AppointmentTimeSlot;
import models.appointment.TimeSlot;
import models.equipment.HospitalEquipment;
import models.renovation.Renovation;
import models.renovation.RenovationRoom;
import models.room.Room;
import models.room.RoomHospitalEquipment;
import models.room.RoomType;
import models.user.User;
import repository.appointment.appointment.AppointmentRepository;
import repository.appointment.appointment_time_slot.AppointmentTimeSlotRepository;
import repository.appointment.time_slot.TimeSlotRepository;
import repository.hospital_equipment.equipment.HospitalEquipmentRepository;
import repository.renovation.renovation.RenovationRepository;
import repository.renovation.renovation_room.RenovationRoomRepository;
import repository.room.room.RoomRepository;
import repository.room.room_equipment.RoomEquipmentRepository;
import service.crud.CRUDService;
import utility.DateValidation;
import utility.validation.RoomValidation;
import utility.validation.Validation;

public class RoomService extends CRUDService<Room, String, RoomRepository> implements IRoomService {

	private RoomRepository roomRepo;

	private RoomEquipmentRepository roomEquRepo;
	private HospitalEquipmentRepository equRepo;
	private AppointmentRepository appointmentRepo;
	private RenovationRepository renovationRepo;
	private RenovationRoomRepository renovationRoomRepo;
	private TimeSlotRepository timeSlotRepo;
	private AppointmentTimeSlotRepository appointmentTimeSlotRepo;

	private DateValidation dateValidation;
	private Validation<Room> roomValidation;

	public RoomService(RoomRepository roomRepo, RoomEquipmentRepository roomEquRepo,
			HospitalEquipmentRepository equRepo, AppointmentRepository appointmentRepo,
			RenovationRoomRepository renovationRoomRepo) {
		super(roomRepo);
		this.roomRepo = roomRepo;
		this.roomEquRepo = roomEquRepo;
		this.equRepo = equRepo;
		this.appointmentRepo = appointmentRepo;
		this.renovationRoomRepo = renovationRoomRepo;
		dateValidation = DateValidation.getInstance();
		roomValidation = new RoomValidation();
	}

	private List<RoomEquDTO> getRoomEquipment(String roomId) throws ResourceNotFound {
		List<RoomEquDTO> equList = new ArrayList<RoomEquDTO>();
		for (RoomHospitalEquipment roomEquipment : roomEquRepo.getAll()) {
			if (roomEquipment.getRoomId().equals(roomId)) {
				if (dateValidation.isDateValid(roomEquipment.getCreatedDate(), roomEquipment.getLastModifiedDate())) {
					HospitalEquipment equipment = equRepo.getById(roomEquipment.getHospitalEquipmentId());
					RoomEquDTO roomEquDTO = new RoomEquDTO(equipment, roomEquipment.getQuantity());
					equList.add(roomEquDTO);
				}
			}
		}
		return equList;
	}

	@Override
	public RoomDTO getRoomWithEquipment(String id) throws ResourceNotFound {
		Room room = getById(id);
		RoomDTO roomDto = new RoomDTO();
		List<RoomEquDTO> equList = getRoomEquipment(room.getId());
		roomDto.setRoom(room);
		roomDto.setEquipmentList(equList);
		return roomDto;
	}

	@Override
	public Room save(Room newRoom) throws InvalidAttributeValueException {
		roomValidation.isDataValid(newRoom);
		for (Room room : this.roomRepo.getAll()) {
			if (room.getId().equals(newRoom.getId())) {
				throw new InvalidAttributeValueException("Soba sa : " + newRoom.getId() + " vec postoji");
			}
		}
		return super.save(newRoom);
	}

	@Override
	public RoomType[] getRoomTypes() {
		return RoomType.values();
	}

	private boolean isRenovationDateValid(DateDTO date) throws InvalidAttributeValueException {
		if (date.getStartDate().equals(date.getEndDate()))
			throw new InvalidAttributeValueException(
					"Pogresno unesen datum renoviranja (datum zavrsetka renoviranja je jednak datumu pocetka renoviranja).");
		if (date.getStartDate().after(date.getEndDate()))
			throw new InvalidAttributeValueException(
					"Pogresno unesen datum renoviranja (datum zavrsetka renoviranja je nakon pocetka).");
		return true;
	}
	
	@Override
	public Room updateRoom(RoomDTO uRoom, DateDTO renovationDate, User user)
			throws ResourceNotFound, InvalidAttributeValueException {
		Room room = getById(uRoom.getRoom().getId());
		if (isRoomReserved(room.getId()))
			throw new InvalidAttributeValueException("Greska. Nije moguce zakazati renoviranje, soba se koristi za zakazane preglede/operacije.");
		isRenovationDateValid(renovationDate);
		if(isRoomInRenovation(room.getId()))
			throw new InvalidAttributeValueException("Greska. Soba ima zakazano renoviranje");
		uRoom.getRoom().setCreatedDate(renovationDate.getStartDate());
		uRoom.getRoom().setLastModifiedDate(null);
		room.setLastModifiedDate(renovationDate.getStartDate());

		//add REnovation
		roomEquRepo.updateRoomEquipment(renovationDate, uRoom);
		super.update(room);
		return super.save(room);
	}
	
	@Override
	public Room mergeRooms(User user, Room room1, Room room2, DateDTO date) throws ResourceNotFound {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Room splitRoom(User user, Room room1, Room room2, DateDTO date) throws ResourceNotFound {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Room delete(Room room) throws ResourceNotFound {
		// dodatno treba proveriti da li neko lezi u sobi ako je soba za lezanje
		if (isRoomReserved(room.getId())) {
			throw new ResourceNotFound("Sobu nije moguce izbrisati zbog zakazanih pregleda");
		}
		if (isRoomInRenovation(room.getId())) {
			throw new ResourceNotFound("Soba nije moguce izbrisati zbog zakazanih renovacija");
		}
		return super.delete(room);
	}

	private boolean isRoomInRenovation(String roomId) {
		for (Renovation renovation : renovationRepo.getScheduledRenovations()) {
			for (RenovationRoom renRoom : renovationRoomRepo.getAll(renovation.getId())) {
				if (renRoom.getRoomId().equals(roomId))
					return true;
			}
		}
		return false;
	}

	private boolean isRoomReserved(String roomId) throws ResourceNotFound {
		List<Appointment> appList = appointmentRepo.getScheduledAppointments(roomId);
		LocalTime currentTime = LocalTime.now();
		for (Appointment appointment : appList) {
			for (AppointmentTimeSlot appTSlot : appointmentTimeSlotRepo.getAll(appointment.getId())) {
				TimeSlot tSlot = timeSlotRepo.getById(appTSlot.getTimeSlotId());
				LocalTime startT = tSlot.getStartTime();
				LocalTime endT = tSlot.getEndTime();
				if (dateValidation.isTimeOverlap(startT, endT) || startT.isAfter(currentTime)) {
					return true;
				}
			}
		}
		return false;
	}

}
