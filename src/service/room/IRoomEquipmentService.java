package service.room;

import models.room.RoomHospitalEquipment;
import repository.generic.GenericRepository;

public interface IRoomEquipmentService extends GenericRepository<RoomHospitalEquipment, Integer>{
}
