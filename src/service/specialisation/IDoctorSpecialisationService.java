/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.specialisation;

import exceptions.ResourceNotFound;
import java.util.List;
import models.user.User;
import models.user.doctor.DoctorSpecialisation;
import models.user.doctor.Specialisation;
import repository.generic.GenericRepository;

/**
 *
 * @author stefan
 */
public interface IDoctorSpecialisationService extends GenericRepository<DoctorSpecialisation, Integer> {

    public List<Specialisation> getByDoctor(Integer doctorId) throws ResourceNotFound;

    public List<User> getDoctors(Integer specId) throws ResourceNotFound;
}
