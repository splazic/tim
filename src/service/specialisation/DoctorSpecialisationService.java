/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.specialisation;

import exceptions.ResourceNotFound;
import java.util.ArrayList;
import java.util.List;
import models.user.User;
import models.user.doctor.Doctor;
import models.user.doctor.DoctorSpecialisation;
import models.user.doctor.Specialisation;
import repository.user.doctor.DoctorSpecialisationRepository;
import repository.user.doctor.SpecialisationFileRepo;
import repository.user.doctor.SpecialisationRepository;
import repository.user.user.UserRepository;
import service.crud.CRUDService;
import utility.FileManager;

/**
 *
 * @author stefan
 */
public class DoctorSpecialisationService extends CRUDService<DoctorSpecialisation, Integer, DoctorSpecialisationRepository> implements IDoctorSpecialisationService {

    private SpecialisationRepository specialisationRepository;
    private UserRepository userRepository;

    public DoctorSpecialisationService(DoctorSpecialisationRepository repo, SpecialisationRepository specialisationRepository, UserRepository userRepository) {
        super(repo);
        this.specialisationRepository = specialisationRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Specialisation> getByDoctor(Integer doctorId) throws ResourceNotFound {
        List<Specialisation> specialisations = new ArrayList<>();
        for (DoctorSpecialisation ds : super.getAll()) {
            if (ds.getDoctorId() == doctorId) {
                Specialisation s = specialisationRepository.getById(ds.getSpecialisationId());
                specialisations.add(s);
            }
        }
        return specialisations;
    }

    public List<User> getDoctors(Integer specId) throws ResourceNotFound {
        List<User> doctors = new ArrayList<>();
        for (DoctorSpecialisation ds : super.getAll()) {
            if (ds.getSpecialisationId() == specId) {
                User d = userRepository.getById(ds.getDoctorId());
                doctors.add(d);
            }
        }
        return doctors;
    }

}
