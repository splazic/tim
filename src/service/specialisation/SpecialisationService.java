/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.specialisation;

import models.user.doctor.Specialisation;
import repository.user.doctor.SpecialisationRepository;
import service.crud.CRUDService;

/**
 *
 * @author stefan
 */
public class SpecialisationService extends CRUDService<Specialisation, Integer, SpecialisationRepository> implements ISpecialisationService {

    public SpecialisationService(SpecialisationRepository repo) {
        super(repo);
    }

}
