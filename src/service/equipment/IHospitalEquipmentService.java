package service.equipment;

import exceptions.ResourceNotFound;
import models.equipment.HospitalEquipment;
import repository.generic.GenericRepository;

public interface IHospitalEquipmentService extends GenericRepository<HospitalEquipment, Integer>{
	int getFreeEquipment(int equipmentId) throws ResourceNotFound;
	public HospitalEquipment delete(HospitalEquipment equipment) throws ResourceNotFound;
	public HospitalEquipment getHospitalEquimpent(String equipmentName) throws ResourceNotFound;
}
