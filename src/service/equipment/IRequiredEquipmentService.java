package service.equipment;

import java.util.List;

import models.equipment.RequiredEquipment;
import repository.generic.GenericRepository;

public interface IRequiredEquipmentService extends GenericRepository<RequiredEquipment, Integer>{
	public List<RequiredEquipment> getRequiredEquipment(int medAppointmentId);
}
