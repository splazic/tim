package service.equipment;

import java.util.ArrayList;
import java.util.List;

import models.equipment.RequiredEquipment;
import repository.hospital_equipment.required_equipment.RequiredEquipmentRepository;
import service.crud.CRUDService;

public class RequiredEquipmentService extends CRUDService<RequiredEquipment, Integer, RequiredEquipmentRepository>
		implements IRequiredEquipmentService {

	public RequiredEquipmentService(RequiredEquipmentRepository repo) {
		super(repo);
	}

	public List<RequiredEquipment> getRequiredEquipment(int medAppointmentId) {
		List<RequiredEquipment> list = new ArrayList<RequiredEquipment>();
		for (RequiredEquipment requiredEquipment : getAll()) {
			if (!requiredEquipment.isDeleted()) {
				if (requiredEquipment.getMedAppointmentTypeId() == medAppointmentId) {
					list.add(requiredEquipment);
				}
			}
		}
		return list;
	}

}
