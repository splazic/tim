package service.equipment;

import java.time.LocalDate;
import java.util.List;

import javax.management.InvalidAttributeValueException;

import exceptions.ResourceNotFound;
import models.appointment.Appointment;
import models.equipment.HospitalEquipment;
import models.equipment.RequiredEquipment;
import models.room.RoomHospitalEquipment;
import repository.appointment.appointment.AppointmentRepository;
import repository.hospital_equipment.equipment.HospitalEquipmentRepository;
import repository.hospital_equipment.required_equipment.RequiredEquipmentRepository;
import repository.room.room_equipment.RoomEquipmentRepository;
import service.crud.CRUDService;
import utility.validation.EquipmentValidation;
import utility.validation.Validation;

public class HospitalEquipmentService extends CRUDService<HospitalEquipment, Integer, HospitalEquipmentRepository>
		implements IHospitalEquipmentService {

	private HospitalEquipmentRepository equipmentRepo;
	private RoomEquipmentRepository roomEquipmentRepo;
	private AppointmentRepository appointmentRepo;
	private RequiredEquipmentRepository requiredEquipmentRepo;

	private Validation<HospitalEquipment> hospitalValidation;

	public HospitalEquipmentService(HospitalEquipmentRepository repo, RoomEquipmentRepository roomEquipmentRepo,
			AppointmentRepository appointmentRepo, RequiredEquipmentRepository requiredEquipmentRepo) {
		super(repo);
		this.equipmentRepo = repo;
		this.roomEquipmentRepo = roomEquipmentRepo;
		this.appointmentRepo = appointmentRepo;
		this.requiredEquipmentRepo = requiredEquipmentRepo;
		this.hospitalValidation = new EquipmentValidation();
	}

	public int getFreeEquipment(int equipmentId) throws ResourceNotFound {
		HospitalEquipment equipment = getById(equipmentId);
		List<RoomHospitalEquipment> list = roomEquipmentRepo.getReservedEquipment(equipment.getId());
		int sum = 0;
		for (RoomHospitalEquipment roomHospitalEquipment : list) {
			sum += roomHospitalEquipment.getQuantity();
		}
		return equipment.getTotalEquipment() - sum;
	}

	@Override
	public HospitalEquipment getHospitalEquimpent(String equipmentName) throws ResourceNotFound{
		HospitalEquipment equipment = equipmentRepo.getByEquipmentName(equipmentName);
		return equipment;
	}
	
	@Override
	public HospitalEquipment save(HospitalEquipment equipment) throws InvalidAttributeValueException {
		try {
			equipmentRepo.getByEquipmentName(equipment.getName());
		} catch (ResourceNotFound e) {
			hospitalValidation.isDataValid(equipment);
			return super.save(equipment);
		}
		throw new InvalidAttributeValueException(
				"Oprema sa nazivom '" + equipment.getName() + "' vec postoji u sistemu.");
	}

	private boolean isQuantityValid(HospitalEquipment equipment, HospitalEquipment updatedEquipment)
			throws ResourceNotFound {
		int freeEqu = getFreeEquipment(equipment.getId());
		int oldTotalEqu = equipment.getTotalEquipment();
		int newTotalEqu = updatedEquipment.getTotalEquipment();
		if ((oldTotalEqu - newTotalEqu) > freeEqu) {
			return false;
		}
		return true;
	}

	private boolean isNameUnique(HospitalEquipment hEquipment, HospitalEquipment uEquipment) {
		if (uEquipment.getName().equals(hEquipment.getName())) {
			try {
				equipmentRepo.getById(uEquipment.getId());
				return false;
			} catch (Exception e) {
			}
		}
		return true;
	}

	@Override
	public HospitalEquipment update(HospitalEquipment uEquipment)
			throws ResourceNotFound, InvalidAttributeValueException {
		HospitalEquipment hEquipment = getById(uEquipment.getId());
		if (!isNameUnique(hEquipment, uEquipment))
			throw new InvalidAttributeValueException("Naziv opreme nije jedinstven.");
		hospitalValidation.isDataValid(uEquipment);
		if (isQuantityValid(hEquipment, uEquipment)) {
			throw new InvalidAttributeValueException("Nije moguce postaviti ukupnu kolicinu opreme na "
					+ uEquipment.getTotalEquipment() + " - potrebno je osloboditi opremu iz prostorija");
		}
		return super.update(uEquipment);
	}

	private boolean isEquipmentInUse(int equipmentId) {
		List<RequiredEquipment> requiredEquList = requiredEquipmentRepo.getAll(equipmentId);
		for (Appointment appointment : appointmentRepo.getScheduledAppointmentsFromDate(LocalDate.now())) {
			for (RequiredEquipment requiredEquipment : requiredEquList) {
				if (appointment.getMedAppointmentTypeId() == requiredEquipment.getMedAppointmentTypeId()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public HospitalEquipment delete(HospitalEquipment equipment) throws ResourceNotFound {
		HospitalEquipment hEquipment = getById(equipment.getId());
		if (isEquipmentInUse(hEquipment.getId()))
			throw new ResourceNotFound("Oprema sa id: [" + equipment.getId() + "] je u upotrebi.");
		roomEquipmentRepo.deleteAll(hEquipment.getId());
		return super.delete(hEquipment);
	}
}
