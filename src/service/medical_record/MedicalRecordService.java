package service.medical_record;

import java.util.List;

import dto.medical_record.MedicalRecordDTO;
import exceptions.ResourceNotFound;
import javax.management.InvalidAttributeValueException;
import models.medical_record.MedicalRecord;
import models.medical_record.MedicalRecordItem;
import repository.medical_record.medical_record.MedicalRecordRepository;
import service.crud.CRUDService;

public class MedicalRecordService extends CRUDService<MedicalRecord, Integer, MedicalRecordRepository> implements IMedicalRecordService{

	private IMedicalRecordItemService medRecItemService;
	
	public MedicalRecordService(MedicalRecordRepository repo, IMedicalRecordItemService medRecItemService) {
		super(repo);
		this.medRecItemService = medRecItemService;
	}

	public MedicalRecordDTO getMedicalRecordWithItems(int medicalRecordId) throws ResourceNotFound {
		MedicalRecord medicalRecord = getById(medicalRecordId);
		List<MedicalRecordItem> medRecordItems = medRecItemService.getAll(medicalRecordId);
		MedicalRecordDTO medicalRecordDTO = new MedicalRecordDTO(medicalRecord, medRecordItems);
		return medicalRecordDTO;
	}

    @Override
    public MedicalRecord save(MedicalRecord object) throws InvalidAttributeValueException {
        return super.save(object); //To change body of generated methods, choose Tools | Templates.
    }
        
        
	
}
