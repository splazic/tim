package service.medical_record;

import java.util.List;
import javax.management.InvalidAttributeValueException;

import models.medical_record.MedicalRecordItem;
import repository.generic.GenericRepository;

public interface IMedicalRecordItemService extends GenericRepository<MedicalRecordItem, Integer>{
	public List<MedicalRecordItem> getAll(int medicalRecordID);
        public MedicalRecordItem save(MedicalRecordItem object) throws InvalidAttributeValueException;
}
