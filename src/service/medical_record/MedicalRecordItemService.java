package service.medical_record;

import java.util.ArrayList;
import java.util.List;
import javax.management.InvalidAttributeValueException;

import models.medical_record.MedicalRecordItem;
import repository.medical_record.medical_rec_item.MedicalRecItemRepository;
import service.crud.CRUDService;
import utility.validation.MedicalRecordItemValidation;
import utility.validation.Validation;

public class MedicalRecordItemService extends CRUDService<MedicalRecordItem, Integer, MedicalRecItemRepository>
        implements IMedicalRecordItemService {

    private Validation<MedicalRecordItem> validation = new MedicalRecordItemValidation();

    public MedicalRecordItemService(MedicalRecItemRepository repo) {
        super(repo);
    }

    public List<MedicalRecordItem> getAll(int medicalRecordID) {
        List<MedicalRecordItem> list = getAll();
        List<MedicalRecordItem> items = new ArrayList<MedicalRecordItem>();
        for (MedicalRecordItem medicalRecordItem : list) {
            if (medicalRecordItem.getMedicalRecordId() == medicalRecordID) {
                if (!medicalRecordItem.isDeleted()) {
                    items.add(medicalRecordItem);
                }
            }
        }
        return items;
    }

    @Override
    public MedicalRecordItem save(MedicalRecordItem object) throws InvalidAttributeValueException {
        validation.isDataValid(object);
        return super.save(object);
    }

}
