package service.medical_record;

import dto.medical_record.MedicalRecordDTO;
import exceptions.ResourceNotFound;
import javax.management.InvalidAttributeValueException;
import models.medical_record.MedicalRecord;
import repository.generic.GenericRepository;

public interface IMedicalRecordService extends GenericRepository<MedicalRecord, Integer>{
	MedicalRecordDTO getMedicalRecordWithItems(int medicalRecordId) throws ResourceNotFound;
        public MedicalRecord save(MedicalRecord object) throws InvalidAttributeValueException;
}
